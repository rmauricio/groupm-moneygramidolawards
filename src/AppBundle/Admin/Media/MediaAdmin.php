<?php

namespace AppBundle\Admin\Media;

use Sonata\AdminBundle\Object\Metadata;
use Sonata\MediaBundle\Provider\MediaProviderInterface;

use Sonata\MediaBundle\Admin\ORM\MediaAdmin as BaseMediaAdmin;

class MediaAdmin extends BaseMediaAdmin
{
    /**
     * {@inheritdoc}
     */
    public function getObjectMetadata($object)
    {
        $provider = $this->pool->getProvider($object->getProviderName());

        $url = $provider->generatePublicUrl(
            $object,
            $provider->getFormatName($object, MediaProviderInterface::FORMAT_ADMIN)
        );

        return new Metadata($object->getName(), $object->getDescription(), $url);
    }
}
