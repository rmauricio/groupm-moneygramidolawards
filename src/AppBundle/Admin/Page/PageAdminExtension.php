<?php

namespace AppBundle\Admin\Page;

use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Admin\AbstractAdminExtension;
use Sonata\AdminBundle\Admin\AdminInterface;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class PageAdminExtension extends AbstractAdminExtension
{
    /**
     * {@inheritdoc}
     */
    public function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Settings')
                ->add('settings', 'sonata_type_immutable_array', [
                    'keys' => array(
                        array('before_html_head_close', TextareaType::class, ['label' => 'Before HTML Head Close', 'required' => false, 'attr' => ['rows' => 5]]),
                        array('after_html_body_open', TextareaType::class, ['label' => 'After HTML Body Open', 'required' => false, 'attr' => ['rows' => 5]]),
                        array('before_html_body_close', TextareaType::class, ['label' => 'Before HTML Body Close', 'required' => false, 'attr' => ['rows' => 5]]),
                    ), 
                    'required' => false, 
                    'label' => false, 
                    'attr' => array()
                ])
            ->end()
        ;
    }
}
