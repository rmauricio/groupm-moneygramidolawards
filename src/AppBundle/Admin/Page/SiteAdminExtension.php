<?php

namespace AppBundle\Admin\Page;

use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Admin\AbstractAdminExtension;
use Sonata\AdminBundle\Admin\AdminInterface;
use Symfony\Component\Form\Extension\Core\Type\LocaleType;
use Sonata\CoreBundle\Form\Type\DateTimePickerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class SiteAdminExtension extends AbstractAdminExtension
{
    /**
     * {@inheritdoc}
     */
    public function configureFormFields(FormMapper $formMapper)
    {
		$formMapper
            ->with('form_site.label_general', ['class' => 'col-md-6'])
                ->add('name')
                ->add('isDefault', null, ['required' => false])
                ->add('enabled', null, ['required' => false])
                ->add('host')
                ->add('locale', LocaleType::class, ['required' => false])
                ->add('relativePath', null, ['required' => false])
                ->add('enabledFrom', DateTimePickerType::class, ['dp_side_by_side' => true])
                ->add('enabledTo', DateTimePickerType::class, ['required' => false, 'dp_side_by_side' => true]
                )
            ->end()
            ->with('form_site.label_seo', ['class' => 'col-md-6'])
                ->add('title', null, ['required' => false])
                ->add('metaDescription', TextareaType::class, ['required' => false])
                ->add('metaKeywords', TextareaType::class, ['required' => false])
            ->end()
			->with('Settings', array('class' => 'col-md-6'))
				->add('settings', 'sonata_type_immutable_array', [
					'keys' => array(
						array(
							$this->getMediaBuilder($formMapper, 'logo', array('btn_add' => 'Add New'), array('link_parameters' => array('provider' => 'sonata.media.provider.image'))), null, array()
						),
                        array('before_html_head_close', TextareaType::class, ['label' => 'Before HTML Head Close', 'required' => false, 'attr' => ['rows' => 5]]),
                        array('after_html_body_open', TextareaType::class, ['label' => 'After HTML Body Open', 'required' => false, 'attr' => ['rows' => 5]]),
                        array('before_html_body_close', TextareaType::class, ['label' => 'Before HTML Body Close', 'required' => false, 'attr' => ['rows' => 5]]),
					), 
					'required' => false, 
					'label' => false, 
					'attr' => array()
				])
			->end()
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function alterObject(AdminInterface $admin, $object)
    {
		$this->subject = $object;
		
        //load logo
        $media = $object->getSetting('logo', null);
        if (is_int($media)) {
            $media = $this->getMediaManager()->findOneBy(['id' => $media]);
        }
        $object->setSetting('logo', $media);
    }

    public function getMediaBuilder(FormMapper $formMapper, $fieldName, $fieldOptions = array(), $fieldExtraOptions = array('link_parameters' => array())) 
    {
        $mediaAdmin = clone $this->getMediaAdmin();
        // simulate an association media...
        $fieldDescription = $mediaAdmin->getModelManager()->getNewFieldDescriptionInstance($mediaAdmin->getClass(), 'media');
        $fieldDescription->setAssociationAdmin($mediaAdmin);
        $fieldDescription->setAdmin($formMapper->getAdmin());
        $fieldDescription->setOption('edit', 'list');
        $fieldDescription->setOptions($fieldExtraOptions);
        $fieldDescription->setAssociationMapping(array(
            'fieldName' => 'media',
            'type'      => \Doctrine\ORM\Mapping\ClassMetadataInfo::MANY_TO_ONE
        ));

        $fieldOptions = array_merge(array(
            'btn_add' => false,
            'sonata_field_description' => $fieldDescription,
            'class'                    => $mediaAdmin->getClass(),
            'model_manager'            => $mediaAdmin->getModelManager()
        ), $fieldOptions);
        
        return $formMapper->create($fieldName, 'sonata_type_model_list', $fieldOptions, $fieldExtraOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function prePersist(AdminInterface $admin, $site)
    {
        $logo = $this->getMediaManager()->findOneBy(array('id' => $site->getSetting('logo')));
        $site->setSetting('logo', is_object($site->getSetting('logo')) ? $site->getSetting('logo')->getId() : null);
        parent::prePersist($admin, $site);
    }

    /**
     * {@inheritdoc}
     */
    public function preUpdate(AdminInterface $admin, $site)
    {
        $logo = $this->getMediaManager()->findOneBy(array('id' => $site->getSetting('logo')));
        
        $site->setSetting('logo', is_object($site->getSetting('logo')) ? $site->getSetting('logo')->getId() : null);
        parent::preUpdate($admin, $site);
    }

    public function getMediaManager()
    {
        return $this->mediaManager;
    }

    public function setMediaManager($mediaManager)
    {
        $this->mediaManager = $mediaManager;
    }

    public function getMediaAdmin()
    {
        return $this->mediaAdmin;
    }

    public function setMediaAdmin($mediaAdmin)
    {
        $this->mediaAdmin = $mediaAdmin;
    }
}
