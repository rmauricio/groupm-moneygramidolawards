<?php

namespace AppBundle\Admin\Promo;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Sonata\CoreBundle\Form\Type\DatePickerType;
use Sonata\AdminBundle\Form\Type\ModelListType;
use Sonata\AdminBundle\Form\Type\ModelType;
use AppBundle\Entity\Promo\DiaryEntry;
use Sonata\FormatterBundle\Form\Type\SimpleFormatterType;

class DiaryEntryAdmin extends AbstractAdmin
{
    protected $datagridValues = array(
        '_page'       => 1,
        '_sort_order' => 'DESC',
        '_sort_by'    => 'createdAt'
    );
	
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $em = $this->modelManager->getEntityManager('Application\Sonata\ClassificationBundle\Entity\Category');
        $query = $em->createQueryBuilder('c')
                    ->select('c')
                    ->from('ApplicationSonataClassificationBundle:Category', 'c')
                    ->where("c.context = 'promo'")
                    ->orderBy('c.position, c.name', 'ASC');

        $datagridMapper
            ->add('id', null, array('label' => 'ID'))
            ->add('promo')
            ->add('category', null, array(), null, array('query_builder' => $query))
            ->add('referenceNumber')
            ->add('firstName', null, array('label' => 'First Name'))
            ->add('lastName', null, array('label' => 'Last Name'))
            ->add('middleName', null, array('label' => 'Middle Name'))
            ->add('title')
            ->add('writeUp', null, array('label' => 'Write-Up'))
            ->add('age')
            ->add('email')
            ->add('mobileNumber')
            ->add('landlineNumber')
            ->add('slug')
            ->add('status', null, array('field_type' => ChoiceType::class), null, array('choices' => array_flip(DiaryEntry::getStatusOptions())))
            ->add('position')
            ->add('createdAt', 'doctrine_orm_datetime_range', ['label' => 'Date Created'], 'sonata_type_datetime_range_picker', [
                'field_options' => [
                    'dp_side_by_side'    => false,
                    //'dp_min_view_mode'   => 'days',
                    'dp_use_current'     => false,
                    'format'             => 'yyyy-MM-dd HH:mm'
                ]
            ])
            ->add('updatedAt', 'doctrine_orm_datetime_range', ['label' => 'Last Update'], 'sonata_type_datetime_range_picker', [
                'field_options' => [
                    'dp_side_by_side'   => false,
                    //'dp_min_view_mode'  => 'days',
                    'dp_use_current'    => false,
                    'format'            => 'yyyy-MM-dd HH:mm'
                ]
            ])
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id', null, array('label' => 'ID'))
            ->add('firstName', null, array('label' => 'First Name'))
            ->add('lastName', null, array('label' => 'Last Name'))
            ->add('image')
            ->add('video')
            ->add('status', 'choice', array('choices' => DiaryEntry::getStatusOptions()))
            ->add('votesCount', null, array('label' => 'Votes'))
            ->add('_action', null, array(
				// 'header_style' => 'width: 10%',
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                ),
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        /*$em = $this->modelManager->getEntityManager('Application\Sonata\ClassificationBundle\Entity\Category');
        $query = $em->createQueryBuilder('c')
                    ->select('c')
                    ->from('ApplicationSonataClassificationBundle:Category', 'c')
                    ->where("c.context = 'promo'")
                    ->andWhere("c.parent IS NOT NULL")
                    ->orderBy('c.position, c.name', 'ASC');*/

        $object = $this->getSubject();

        if ($object->getId()) {
            $formMapper
                ->tab('General') // the tab call is optional
                    ->with('', array('class' => 'col-md-12'))
                        ->add('referenceNumberData', TextType::class, array('attr' => array('readonly' => 'readonly'), 'required' => false, 'mapped' => false, 'data' => $object->getReferenceNumber()))
                    ->end()
                ->end()
            ;
        }

        $formMapper
            ->tab('General')
                ->with('', array('class' => 'col-md-12'))
                    ->add('promo', ModelType::class, array(), array())
                    ->add('category', ModelListType::class, array('required' => false, 'btn_edit' => false, 'btn_add' => false), array('link_parameters' => array('context' => 'promo')))
                    ->add('title')
                    ->add('abstract', SimpleFormatterType::class, array(
                        'required' => false,
                        'format' => 'richhtml',
                        'ckeditor_context' => 'default'
                    ))
                    ->add('writeUp', TextareaType::class, array('label' => 'Write-Up'))
                ->end()
            ->end()
        ;

        if ($object->getId()) {
            $formMapper
                ->tab('General')
                    ->with('', array('class' => 'col-md-12'))
                        ->add('slug')
                        ->add('votesCountData', TextType::class, array('label' => 'Votes', 'attr' => array('readonly' => 'readonly'), 'required' => false, 'mapped' => false, 'data' => $object->getVotesCount()))
                    ->end()
                ->end()
            ;
        }

        $formMapper
            ->tab('General')
                ->with('', array('class' => 'col-md-12'))
                    ->add('status', ChoiceType::class, array('choices' => array_flip(DiaryEntry::getStatusOptions())))
                    ->add('position')
                ->end()
            ->end()
            ->tab('Media')
                ->with('', array('class' => 'col-md-12'))
                    ->add('image', ModelListType::class, array('required' => false, 'btn_edit' => false), array('link_parameters' => array('provider' => 'sonata.media.provider.image')))
                    ->add('video', ModelListType::class, array('required' => false, 'btn_edit' => false), array('link_parameters' => array('provider' => 'sonata.media.provider.file')))
                    ->add('thumbnail', ModelListType::class, array('required' => false, 'btn_edit' => false), array('link_parameters' => array('provider' => 'sonata.media.provider.image')))
                    ->add('finalImage', ModelListType::class, array('required' => false, 'btn_edit' => false), array('link_parameters' => array('provider' => 'sonata.media.provider.image')))
                    ->add('finalImageMobile', ModelListType::class, array('required' => false, 'btn_edit' => false), array('link_parameters' => array('provider' => 'sonata.media.provider.image')))
                    ->add('finalVideo', ModelListType::class, array('required' => false, 'btn_edit' => false), array('link_parameters' => array('provider' => 'sonata.media.provider.youtube')))
                ->end()
            ->end()
            ->tab('Personal Information') // the tab call is optional
                ->with('', array('class' => 'col-md-12'))
                    ->add('firstName', null, array('label' => 'First Name'))
                    ->add('middleName', null, array('label' => 'Middle Name', 'required' => false))
                    ->add('lastName', null, array('label' => 'Last Name'))
                    ->add('email')
                    ->add('mobileNumber')
                    ->add('landlineNumber', null, array('required' => false))
                    ->add('age')
                ->end()
            ->end()
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
		$showMapper
            ->tab('General')
                ->with('', array('class' => 'col-md-12'))
                    ->add('id', null, array('label' => 'ID'))
                    ->add('promo')
                    ->add('category')
                    ->add('referenceNumber')
                    ->add('title')
                    ->add('abstract')
                    ->add('writeUp', null, array('label' => 'Write-Up'))
                    ->add('slug')
                    ->add('votesCount', null, array('label' => 'Votes'))
                    ->add('status', 'choice', array('choices' => DiaryEntry::getStatusOptions()))
                    ->add('position')
                    ->add('updatedAt', null, array('label' => 'Last Update'))
                    ->add('createdAt', null, array('label' => 'Date Created'))
                ->end()
            ->end()
            ->tab('Media')
                ->with('', array('class' => 'col-md-12'))
                    ->add('image')
                    ->add('video')
                    ->add('thumbnail')
                    ->add('finalImage')
                    ->add('finalImageMobile')
                    ->add('finalVideo')
                ->end()
            ->end()
            ->tab('Personal Information')
                ->with('', array('class' => 'col-md-12'))
                    ->add('firstName', null, array('label' => 'First Name'))
                    ->add('middleName', null, array('label' => 'Middle Name'))
                    ->add('lastName', null, array('label' => 'Last Name'))
                    ->add('email')
                    ->add('mobileNumber')
                    ->add('landlineNumber')
                    ->add('age')
                ->end()
            ->end()
		;
	}

    public function getExportFormats()
    {
        return array(
            'csv', 'xls',
        );
    }

    public function getExportFields()
    {
        return array(
            'ID' => 'id',
            'Promo' => 'promo.name',
            'Category' => 'category.name',
            'Reference Number' => 'referenceNumber',
            'Image' => 'image.name',
            'Video' => 'video.name',
            'Thumbnail' => 'thumbnail.name',
            'Final Image' => 'finalImage.name',
            'Final Image Mobile' => 'finalImageMobile.name',
            'Final Video' => 'finalVideo.name',
            'Title' => 'title',
            'Write-Up' => 'writeUp',
            'First Name' => 'firstName',
            'Middle Name' => 'middleName',
            'Last Name' => 'lastName',
            'Email' => 'email',
            'Mobile Number' => 'mobileNumber',
            'Landline Number' => 'landlineNumber',
            'Age' => 'age',
            'Slug' => 'slug',
            'Status' => 'getStatusLabel',
            'Position' => 'position',
            'Last Update' => 'updatedAt',
            'Date Created' => 'createdAt',
        );
    }
}
