<?php

namespace AppBundle\Admin\Promo;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Sonata\CoreBundle\Form\Type\DatePickerType;
use Sonata\AdminBundle\Form\Type\ModelListType;
use Sonata\AdminBundle\Form\Type\ModelType;
use AppBundle\Entity\Promo\DiaryEntry;
use Sonata\FormatterBundle\Form\Type\SimpleFormatterType;

class DiaryEntryVoteAdmin extends AbstractAdmin
{
    protected $datagridValues = array(
        '_page'       => 1,
        '_sort_order' => 'ASC',
        '_sort_by'    => 'id'
    );

    protected $parentAssociationMapping = 'diaryEntry.promo';
	
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id', null, array('label' => 'ID'))
            ->add('email')
            ->add('diaryEntry.referenceNumber', null, array('label' => 'Entry Reference Number'))
            ->add('diaryEntry.promo', null, array('label' => 'Promo'))
            ->add('createdAt', 'doctrine_orm_datetime_range', ['label' => 'Date Created'], 'sonata_type_datetime_range_picker', [
                'field_options' => [
                    'dp_side_by_side'    => false,
                    //'dp_min_view_mode'   => 'days',
                    'dp_use_current'     => false,
                    'format'             => 'yyyy-MM-dd HH:mm'
                ]
            ])
            ->add('updatedAt', 'doctrine_orm_datetime_range', ['label' => 'Last Update'], 'sonata_type_datetime_range_picker', [
                'field_options' => [
                    'dp_side_by_side'   => false,
                    //'dp_min_view_mode'  => 'days',
                    'dp_use_current'    => false,
                    'format'            => 'yyyy-MM-dd HH:mm'
                ]
            ])
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id', null, array('label' => 'ID'))
            ->add('email')
            ->add('diaryEntry')
			->add('createdAt', null, array('label' => 'Date Created'))
            /*->add('_action', null, array(
				// 'header_style' => 'width: 10%',
                'actions' => array(
                    'show' => array(),
                    // 'edit' => array(),
                    // 'delete' => array(),
                ),
            ))*/
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $object = $this->getSubject();

        $formMapper
            ->with('General', array('class' => 'col-md-12'))
                ->add('diaryEntry', ModelListType::class, array('required' => false, 'btn_edit' => false, 'btn_add' => false), array('link_parameters' => array()))
                ->add('email')
            ->end()
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
		$showMapper
            ->with('General', array('class' => 'col-md-12'))
                ->add('diaryEntry', null, array())
                ->add('email')
            ->end()
		;
	}

    public function getExportFormats()
    {
        return array(
            'csv', 'xls',
        );
    }

    public function getExportFields()
    {
        return array(
            'ID' => 'id',
            'Diary Entry' => 'diaryEntry',
            'Email' => 'email',
            'Last Update' => 'updatedAt',
            'Date Created' => 'createdAt',
        );
    }
}
