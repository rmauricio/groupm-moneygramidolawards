<?php

namespace AppBundle\Admin\Promo;

use Knp\Menu\ItemInterface as MenuItemInterface;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Sonata\CoreBundle\Form\Type\DatePickerType;
use Sonata\CoreBundle\Form\Type\DateTimePickerType;
use Sonata\AdminBundle\Form\Type\ModelListType;
use AppBundle\Entity\Promo\Promo;
use Cocur\Slugify\Slugify;

class PromoAdmin extends AbstractAdmin
{
    protected $datagridValues = array(
        '_page'       => 1,
        '_sort_order' => 'DESC',
        '_sort_by'    => 'enabled'
    );

    /*protected function configureSideMenu(MenuItemInterface $menu, $action, AdminInterface $childAdmin = null)
    {
        if (!$childAdmin && !in_array($action, ['edit', 'show'])) {
            return;
        }

        $admin = $this->isChild() ? $this->getParent() : $this;
        $id = $admin->getRequest()->get('id');

        $menu->addChild('View Promo', [
            'uri' => $admin->generateUrl('show', ['id' => $id])
        ]);

        if ($this->isGranted('EDIT')) {
            $menu->addChild('Edit Promo', [
                'uri' => $admin->generateUrl('edit', ['id' => $id])
            ]);
        }

        if ($this->isGranted('LIST')) {
            $menu->addChild('Votes', [
                'uri' => $admin->generateUrl('app.admin.promo.diary_entry_vote.list', ['id' => $id])
            ]);
        }
    }*/
	
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id', null, array('label' => 'ID'))
            ->add('type', null, array('field_type' => ChoiceType::class), null, array('choices' => array_flip(Promo::getTypeOptions())))
            ->add('name')
            ->add('publishStart')
            ->add('publishEnd')
            ->add('slug')
            ->add('enabled')
            // ->add('displayWinners')
            ->add('optionDisplayName')
            // ->add('menuDisplayName', null, array('label' => 'Menu Winners Display Name'))
            ->add('createdAt', 'doctrine_orm_datetime_range', ['label' => 'Date Created'], 'sonata_type_datetime_range_picker', [
                'field_options' => [
                    'dp_side_by_side'    => false,
                    //'dp_min_view_mode'   => 'days',
                    'dp_use_current'     => false,
                    'format'             => 'yyyy-MM-dd HH:mm'
                ]
            ])
            ->add('updatedAt', 'doctrine_orm_datetime_range', ['label' => 'Last Update'], 'sonata_type_datetime_range_picker', [
                'field_options' => [
                    'dp_side_by_side'   => false,
                    //'dp_min_view_mode'  => 'days',
                    'dp_use_current'    => false,
                    'format'            => 'yyyy-MM-dd HH:mm'
                ]
            ])
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id', null, array('label' => 'ID'))
            ->add('name')
            ->add('type', 'choice', array('choices' => Promo::getTypeOptions()))
            ->add('publishStart')
            ->add('publishEnd')
            ->add('enabled')
			->add('createdAt', null, array('label' => 'Date Created'))
            ->add('_action', null, array(
				// 'header_style' => 'width: 10%',
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                ),
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $object = $this->getSubject();

        $formMapper
            ->add('type', ChoiceType::class, array('choices' => array_flip(Promo::getTypeOptions())))
            ->add('name')
            ->add('publishStart', DateTimePickerType::class, array(
                'format' => 'yyyy-MM-dd H:mm',
            ))
            ->add('publishEnd', DateTimePickerType::class, array(
                'format' => 'yyyy-MM-dd H:mm',
            ))
        ;

        if ($object->getId()) {
            $formMapper->add('slug');
        }

        $formMapper
            ->add('optionDisplayName')
            //->add('menuDisplayName', null, array('label' => 'Menu Winners Display Name'))
            //->add('displayWinners')
            ->add('enabled')
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
		$showMapper
            ->add('type', 'choice', array('choices' => Promo::getTypeOptions()))
            ->add('name')
            ->add('publishStart')
            ->add('publishEnd')
            ->add('slug')
            ->add('optionDisplayName')
            //->add('menuDisplayName', null, array('label' => 'Menu Winners Display Name'))
            //->add('displayWinners')
            ->add('enabled')
            ->add('updatedAt', null, array('label' => 'Last Update'))
            ->add('createdAt', null, array('label' => 'Date Created'))
		;
	}

    public function getExportFormats()
    {
        return array(
            'csv', 'xls',
        );
    }

    public function getExportFields()
    {
        return array(
            'ID' => 'id',
            'Type' => 'getTypeLabel',
            'Name' => 'name',
            'Publish Start' => 'publishStart',
            'Publish End' => 'publishEnd',
            'Slug' => 'slug',
            'Option Display Name' => 'optionDisplayName',
            //'Menu Winners Display Name' => 'menuDisplayName',
            //'Display Winners' => 'displayWinners',
            'Enabled' => 'enabled',
            'Last Update' => 'updatedAt',
            'Date Created' => 'createdAt',
        );
    }
}
