<?php

namespace AppBundle\Admin\Promo;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Sonata\CoreBundle\Form\Type\DatePickerType;
use Sonata\AdminBundle\Form\Type\ModelListType;
use Sonata\AdminBundle\Form\Type\ModelType;
use AppBundle\Entity\Promo\RemittanceEntry;
use Sonata\FormatterBundle\Form\Type\SimpleFormatterType;

class RemittanceEntryAdmin extends AbstractAdmin
{
    protected $datagridValues = array(
        '_page'       => 1,
        '_sort_order' => 'DESC',
        '_sort_by'    => 'createdAt'
    );
    
    /*protected function configureRoutes(RouteCollection $collection) {
        $collection->remove("edit");
        parent::configureRoutes($collection);
    }*/
	
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $em = $this->modelManager->getEntityManager('Application\Sonata\ClassificationBundle\Entity\Category');
        $query = $em->createQueryBuilder('c')
                    ->select('c')
                    ->from('ApplicationSonataClassificationBundle:Category', 'c')
                    ->where("c.context = 'promo'")
                    ->orderBy('c.position, c.name', 'ASC');

        $datagridMapper
            ->add('id', null, array('label' => 'ID'))
            ->add('promo')
            ->add('category', null, array(), null, array('query_builder' => $query))
            ->add('transactionNumber')
            ->add('referenceNumber')
            ->add('status', null, array('field_type' => ChoiceType::class), null, array('choices' => array_flip(RemittanceEntry::getStatusOptions())))
            ->add('position')
            ->add('isSender')
            // ->add('senderRegion')
            ->add('firstName', null, array('label' => 'First Name'))
            ->add('lastName', null, array('label' => 'Last Name'))
            ->add('middleName', null, array('label' => 'Middle Name'))
            ->add('age')
            ->add('email')
            ->add('mobileNumber')
            ->add('landlineNumber')
            ->add('country', null, array('field_type' => CountryType::class), null, array())
            ->add('recipientCountry', null, array('field_type' => CountryType::class), null, array())
            ->add('recipientAddress1', null, array('label' => 'Recipient House/Lot Number'))
            ->add('recipientAddress2', null, array('label' => 'Recipient Street'))
            ->add('recipientAddress3', null, array('label' => 'Recipient Barangay/ State/ Province'))
            ->add('recipientRegion')
            ->add('recipientCity')
            ->add('recipientZipCode')
            ->add('title')
            ->add('createdAt', 'doctrine_orm_datetime_range', ['label' => 'Date Created'], 'sonata_type_datetime_range_picker', [
                'field_options' => [
                    'dp_side_by_side'    => false,
                    //'dp_min_view_mode'   => 'days',
                    'dp_use_current'     => false,
                    'format'             => 'yyyy-MM-dd HH:mm'
                ]
            ])
            ->add('updatedAt', 'doctrine_orm_datetime_range', ['label' => 'Last Update'], 'sonata_type_datetime_range_picker', [
                'field_options' => [
                    'dp_side_by_side'   => false,
                    //'dp_min_view_mode'  => 'days',
                    'dp_use_current'    => false,
                    'format'            => 'yyyy-MM-dd HH:mm'
                ]
            ])
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id', null, array('label' => 'ID'))
            ->addIdentifier('transactionNumber')
            ->add('firstName', null, array('label' => 'First Name'))
            ->add('lastName', null, array('label' => 'Last Name'))
            ->add('status', 'choice', array('choices' => RemittanceEntry::getStatusOptions()))
			->add('createdAt', null, array('label' => 'Date Created'))
            ->add('_action', null, array(
				// 'header_style' => 'width: 10%',
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                ),
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        /*$em = $this->modelManager->getEntityManager('Application\Sonata\ClassificationBundle\Entity\Category');
        $query = $em->createQueryBuilder('c')
                    ->select('c')
                    ->from('ApplicationSonataClassificationBundle:Category', 'c')
                    ->where("c.context = 'promo'")
                    ->andWhere("c.parent IS NOT NULL")
                    ->orderBy('c.position, c.name', 'ASC');*/

        $object = $this->getSubject();

        $formMapper
            ->tab('General') // the tab call is optional
                ->with('', array('class' => 'col-md-12'))
                    ->add('transactionNumber')
                ->end()
            ->end()
        ;

        if ($object->getId()) {
            $formMapper
                ->tab('General') // the tab call is optional
                    ->with('', array('class' => 'col-md-12'))
                        ->add('referenceNumberData', TextType::class, array('attr' => array('readonly' => 'readonly'), 'required' => false, 'mapped' => false, 'data' => $object->getReferenceNumber()))
                    ->end()
                ->end()
            ;
        }

        $formMapper
            ->tab('General') // the tab call is optional
                ->with('', array('class' => 'col-md-12'))
                    ->add('promo', ModelType::class, array(), array())
                    ->add('category', ModelListType::class, array('required' => false, 'btn_edit' => false, 'btn_add' => false), array('link_parameters' => array('context' => 'promo')))
                    ->add('image', ModelListType::class, array('required' => false, 'btn_edit' => false), array('link_parameters' => array('provider' => 'sonata.media.provider.image')))
                    ->add('thumbnail', ModelListType::class, array('required' => false, 'btn_edit' => false), array('link_parameters' => array('provider' => 'sonata.media.provider.image')))
                    ->add('title')
                    ->add('abstract', SimpleFormatterType::class, array(
                        'required' => false,
                        'format' => 'richhtml',
                        'ckeditor_context' => 'default'
                    ))
                    ->add('position')
                ->end()
            ->end()
        ;

        $formMapper
            ->tab('General') // the tab call is optional
                ->with('', array('class' => 'col-md-12'))
                    ->add('isSender', null, array('required' => false))
                    // ->add('senderRegion')
                    ->add('status', ChoiceType::class, array('choices' => array_flip(RemittanceEntry::getStatusOptions())))
                ->end()
            ->end()
        ;

        $formMapper
            ->tab('Personal Information') // the tab call is optional
                ->with('', array('class' => 'col-md-12'))
                    ->add('firstName', null, array('label' => 'First Name'))
                    ->add('middleName', null, array('label' => 'Middle Name', 'required' => false))
                    ->add('lastName', null, array('label' => 'Last Name'))
                    ->add('email')
                    ->add('mobileNumber')
                    ->add('landlineNumber', null, array('required' => false))
                    ->add('age')
                    ->add('country', CountryType::class, array())
                ->end()
            ->end()
            ->tab('Recipient Information') // the tab call is optional
                ->with('', array('class' => 'col-md-12'))
                    ->add('recipientCountry', CountryType::class, array())
                    ->add('recipientAddress1', null, array('label' => 'Recipient House/Lot Number'))
                    ->add('recipientAddress2', null, array('label' => 'Recipient Street'))
                    ->add('recipientAddress3', null, array('label' => 'Recipient Barangay/ State/ Province'))
                    ->add('recipientRegion')
                    ->add('recipientCity')
                    ->add('recipientZipCode')
                ->end()
            ->end()
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
		$showMapper
            ->tab('General') // the tab call is optional
                ->with('', array('class' => 'col-md-12'))
                    ->add('id', null, array('label' => 'ID'))
                    ->add('transactionNumber')
                    ->add('referenceNumber')
                    ->add('promo')
                    ->add('category')
                    ->add('image')
                    ->add('isSender')
                    // ->add('senderRegion')
                    ->add('status', 'choice', array('choices' => RemittanceEntry::getStatusOptions()))
                    ->add('updatedAt', null, array('label' => 'Last Update'))
                    ->add('createdAt', null, array('label' => 'Date Created'))
                ->end()
            ->end()
            ->tab('Personal Information') // the tab call is optional
                ->with('', array('class' => 'col-md-12'))
                    ->add('firstName', null, array('label' => 'First Name'))
                    ->add('middleName', null, array('label' => 'Middle Name'))
                    ->add('lastName', null, array('label' => 'Last Name'))
                    ->add('email')
                    ->add('mobileNumber')
                    ->add('landlineNumber')
                    ->add('age')
                    ->add('countryLabel', null, array('label' => 'Country'))
                ->end()
            ->end()
            ->tab('Recipient Information') // the tab call is optional
                ->with('', array('class' => 'col-md-12'))
                    ->add('recipientCountryLabel', null, array('label' => 'Recipient Country'))
                    ->add('recipientAddress1', null, array('label' => 'Recipient House/Lot Number'))
                    ->add('recipientAddress2', null, array('label' => 'Recipient Street'))
                    ->add('recipientAddress3', null, array('label' => 'Recipient Barangay/ State/ Province'))
                    ->add('recipientRegion')
                    ->add('recipientCity')
                    ->add('recipientZipCode')
                ->end()
            ->end()
		;
	}

    public function getExportFormats()
    {
        return array(
            'csv', 'xls',
        );
    }

    public function getExportFields()
    {
        return array(
            'ID' => 'id',
            'Promo' => 'promo.name',
            'Category' => 'category.name',
            'Image' => 'image.name',
            'Transaction Number' => 'transactionNumber',
            'Reference Number' => 'referenceNumber',
            'Is Sender' => 'getIsSenderLabel',
            //'Sender Region' => 'senderRegion',
            'Status' => 'getStatusLabel',
            'First Name' => 'firstName',
            'Middle Name' => 'middleName',
            'Last Name' => 'lastName',
            'Email' => 'email',
            'Mobile Number' => 'mobileNumber',
            'Landline Number' => 'landlineNumber',
            'Age' => 'age',
            'Country' => 'getCountryLabel',
            'Recipient Country' => 'getRecipientCountryLabel',
            'Recipient House/Lot Number' => 'recipientAddress1',
            'Recipient Street' => 'recipientAddress2',
            'Recipient Barangay/ State/ Province' => 'recipientAddress3',
            'Recipient Region' => 'recipientRegion',
            'Recipient City' => 'recipientCity',
            'Recipient Zip Code' => 'recipientZipCode',
            'Last Update' => 'updatedAt',
            'Date Created' => 'createdAt',
        );
    }
}
