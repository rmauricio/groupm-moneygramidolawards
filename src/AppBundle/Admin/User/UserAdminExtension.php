<?php

namespace AppBundle\Admin\User;

use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Admin\AbstractAdminExtension;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\CoreBundle\Form\Type\DatePickerType;

use Doctrine\ORM\EntityRepository;

class UserAdminExtension extends AbstractAdminExtension
{	
    /**
     * {@inheritdoc}
     */
    public function configureListFields(ListMapper $listMapper)
    {
		parent::configureListFields($listMapper);

		$listMapper->remove('createdAt');
		$listMapper->remove('impersonating');

		$listMapper->add('createdAt', null, array('label' => 'Date Created'));
    }
	
    /**
     * {@inheritdoc}
     */
    public function configureFormFields(FormMapper $formMapper)
    {
		parent::configureFormFields($formMapper);

		$now = new \DateTime();

		$formMapper->remove('realRoles');
		$formMapper->remove('dateOfBirth');

		$formMapper
			->tab('User')
	            ->with('Profile')
	                ->add('dateOfBirth', DatePickerType::class, [
	                    'years' => range(1900, $now->format('Y')),
	                    'format' => 'yyyy-MM-dd',
	                    'dp_min_date' => '1-1-1900',
	                    'dp_max_date' => $now->format('c'),
	                    'required' => false,
	                ])
	            ->end()
            ->end()
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureShowFields(ShowMapper $showMapper)
    {
    	parent::configureShowFields($showMapper);
    }

    /**
     * {@inheritdoc}
     */
    public function prePersist(AdminInterface $admin, $object)
    {
    	parent::prePersist($admin, $object);
    }

    /**
     * {@inheritdoc}
     */
    public function preUpdate(AdminInterface $admin, $object)
    {
    	parent::preUpdate($admin, $object);
    }

}
