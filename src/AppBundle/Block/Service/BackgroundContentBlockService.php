<?php

namespace AppBundle\Block\Service;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use Sonata\BlockBundle\Model\BlockInterface;
use Sonata\BlockBundle\Block\BlockContextInterface;

use Sonata\AdminBundle\Form\FormMapper;
use Sonata\CoreBundle\Validator\ErrorElement;
use Sonata\BlockBundle\Block\Service\AbstractAdminBlockService;
use Sonata\CoreBundle\Model\Metadata;
use Symfony\Component\Form\FormBuilderInterface;

use Doctrine\ORM\Mapping\ClassMetadataInfo;
use Sonata\CoreBundle\Model\ManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Templating\EngineInterface;
use Sonata\MediaBundle\Model\MediaInterface;
use Sonata\MediaBundle\Provider\Pool;
use Sonata\FormatterBundle\Form\Type\FormatterType;

/**
 * @author Ryan Mauricio <ryanmarkrmauricio@gmail.com>
 */
class BackgroundContentBlockService extends AbstractAdminBlockService 
{
	/**
     * @var ManagerInterface
     */
    protected $mediaAdmin;

    /**
     * @var ManagerInterface
     */
    protected $mediaManager;

    /**
     * @param string             $name
     * @param EngineInterface    $templating
     * @param ContainerInterface $container
     * @param ManagerInterface   $mediaManager
     */
    public function __construct($name, EngineInterface $templating, ContainerInterface $container, ManagerInterface $mediaManager)
    {
        parent::__construct($name, $templating);

        $this->mediaManager = $mediaManager;
        $this->container = $container;
    }

	/**
     * {@inheritdoc}
     */
    public function execute(BlockContextInterface $blockContext, Response $response = null)
    {
    	$settings = $blockContext->getSettings();
    	$layouts = self::getLayoutTemplates();

        return $this->renderResponse($layouts[$settings['layout']], array(
            'block' => $blockContext->getBlock(),
            'settings' => $settings,
        ), $response);
    }

    /**
     * {@inheritdoc}
     */
    public function buildEditForm(FormMapper $formMapper, BlockInterface $block)
    {
    	if (!$block->getSetting('mediaId') instanceof MediaInterface) {
            $this->load($block);
        }
        
        $backgroundImageFormatChoices = $this->getFormatChoices((!empty($block->getSetting('backgroundImage')) ? $block->getSetting('backgroundImage') : null));
        $backgroundImageMobileormatChoices = $this->getFormatChoices((!empty($block->getSetting('backgroundImageMobile')) ? $block->getSetting('backgroundImageMobile') : null));

        // NEXT_MAJOR: Keep FQCN when bumping Symfony requirement to 2.8+.
        if (method_exists('Symfony\Component\Form\AbstractType', 'getBlockPrefix')) {
            $immutableArrayType = 'Sonata\CoreBundle\Form\Type\ImmutableArrayType';
            $textType = 'Symfony\Component\Form\Extension\Core\Type\TextType';
            $choiceType = 'Symfony\Component\Form\Extension\Core\Type\ChoiceType';
        } else {
            $immutableArrayType = 'sonata_type_immutable_array';
            $textType = 'text';
            $choiceType = 'choice';
        }

        $formMapper->add('settings', 'sonata_type_immutable_array', array(
            'keys' => array(
                array('content', FormatterType::class, function (FormBuilderInterface $formBuilder) {
                    return array(
                        'event_dispatcher' => $formBuilder->getEventDispatcher(),
                        'format_field' => array('format', '[format]'),
                        'source_field' => array('rawContent', '[rawContent]'),
                        'target_field' => '[content]',
                        'label' => 'Content',
                        //'required' => false,
                    );
                }),
                array('contentBottom', FormatterType::class, function (FormBuilderInterface $formBuilder) {
                    return array(
                        'event_dispatcher' => $formBuilder->getEventDispatcher(),
                        'format_field' => array('format', '[format]'),
                        'source_field' => array('rawContentBottom', '[rawContentBottom]'),
                        'target_field' => '[contentBottom]',
                        'label' => 'Content Bottom',
                        //'required' => false,
                    );
                }),
                array($this->getMediaBuilder('backgroundImage', $formMapper, array('label' => 'Background Image')), null, array()),
                array(
                    'backgroundImageFormat', $choiceType, array(
	                    'required' => count($backgroundImageFormatChoices) > 0,
	                    'choices' => array_flip($backgroundImageFormatChoices),
	                    'label' => 'Background Image Format',
	                    'empty_data' => null,
	                    'placeholder' => 'reference',
	                )
                ),
                array($this->getMediaBuilder('backgroundImageMobile', $formMapper, array('label' => 'Background Image (Mobile)', 'required' => false)), null, array()),
                array(
                	'backgroundImageMobileFormat', $choiceType, array(
	                    'required' => count($backgroundImageMobileormatChoices) > 0,
	                    'choices' => array_flip($backgroundImageMobileormatChoices),
	                    'label' => 'Background Image Format (Mobile)',
	                    'empty_data' => null,
	                    'placeholder' => 'reference',
	                )
                ),
                array(
                    'backgroundImageLocation', $choiceType, array(
	                    'required' => false,
	                    'choices' => array_flip(array('wrapper' => 'Wrapper', 'inside_wrapper' => 'Inside Wrapper')),
	                    'label' => 'Background Image Location',
	                )
	            ),
                /*array(
                    'htmlElement', $choiceType, array(
	                    'required' => false,
	                    'choices' => array('div' => 'div', 'section' => 'section'),
	                    'label' => 'Html Element',
	                )
                ),*/
                array(
                    'class',
                    $textType,
                    array(
                        'required' => false,
                        'label' => 'Class',
                    )
                ),
                array(
                    'innerClass',
                    $textType,
                    array(
                        'required' => false,
                        'label' => 'Inner Class',
                    )
                ),
                array(
                	'customStyles',
                	'textarea',
                	array(
                		'required' => false,
                		'label' => 'Custom Styles',
                        'attr' => array(
                            'rows' => 15
                        ),
                	)
                ),
                array(
                    'layout', $choiceType, array(
	                    'required' => true,
	                    'choices' => array_flip(self::getLayoutOptions()),
	                    'label' => 'Layout',
	                )
                ),
            ),
            // 'translation_domain' => 'SonataFormatterBundle',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function configureSettings(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'context' => false,
            'backgroundImage' => null,
            'backgroundImageFormat' => null,
            'backgroundImageLocation' => 'wrapper',
            'htmlElement' => 'div',
            'backgroundImageMobile' => null,
            'backgroundImageMobileFormat' => null,
            'class' => null,
            'innerClass' => null,
            'customStyles' => null,
            'customStylesMobile' => null,
            'layout' => 'default',
            'format' => 'richhtml',
            'rawContent' => '<b>Insert your custom content here</b>',
            'content' => '<b>Insert your custom content here</b>',
            'rawContentBottom' => '<b>Insert your custom content here</b>',
            'contentBottom' => '<b>Insert your custom content here</b>',
            'template' => 'AppBundle:Block:background_content.html.twig',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function load(BlockInterface $block)
    {
        $media = $block->getSetting('backgroundImage', null);

        if (is_int($media)) {
            $media = $this->mediaManager->findOneBy(array('id' => $media));
        }

        $block->setSetting('backgroundImage', $media);

        $media = $block->getSetting('backgroundImageMobile', null);

        if (is_int($media)) {
            $media = $this->mediaManager->findOneBy(array('id' => $media));
        }

        $block->setSetting('backgroundImageMobile', $media);
    }

    /**
     * {@inheritdoc}
     */
    public function prePersist(BlockInterface $block)
    {
        $block->setSetting('backgroundImage', is_object($block->getSetting('backgroundImage')) ? $block->getSetting('backgroundImage')->getId() : null);
        $block->setSetting('backgroundImageMobile', is_object($block->getSetting('backgroundImageMobile')) ? $block->getSetting('backgroundImageMobile')->getId() : null);
    }

    /**
     * {@inheritdoc}
     */
    public function preUpdate(BlockInterface $block)
    {
        $block->setSetting('backgroundImage', is_object($block->getSetting('backgroundImage')) ? $block->getSetting('backgroundImage')->getId() : null);
        $block->setSetting('backgroundImageMobile', is_object($block->getSetting('backgroundImageMobile')) ? $block->getSetting('backgroundImageMobile')->getId() : null);
    }

    public function getBlockMetadata($code = null): Metadata
    {
        return new Metadata(
            $this->getName(),
            null !== $code ? $code : $this->getName(),
            false,
            'AppBundle',
            ['class' => 'fa fa-file-text-o']
        );
    }

    /**
     * @param MediaInterface|null $media
     *
     * @return array
     */
    protected function getFormatChoices(MediaInterface $media = null)
    {
        $formatChoices = array();

        if (!$media instanceof MediaInterface) {
            return $formatChoices;
        }

        $formats = $this->getMediaPool()->getFormatNamesByContext($media->getContext());

        foreach ($formats as $code => $format) {
            $formatChoices[$code] = $code;
        }

        return $formatChoices;
    }

    /**
     * @return Pool
     */
    public function getMediaPool()
    {
        return $this->getMediaAdmin()->getPool();
    }

    /**
     * @return BaseMediaAdmin
     */
    public function getMediaAdmin()
    {
        if (!$this->mediaAdmin) {
            $this->mediaAdmin = $this->container->get('sonata.media.admin.media');
        }

        return $this->mediaAdmin;
    }

    /**
     * @param FormMapper $formMapper
     *
     * @return FormBuilder
     */
    protected function getMediaBuilder($fieldName, FormMapper $formMapper, $options = array())
    {
        // simulate an association ...
        $fieldDescription = $this->getMediaAdmin()->getModelManager()->getNewFieldDescriptionInstance($this->mediaAdmin->getClass(), 'media', array(
            'translation_domain' => 'SonataMediaBundle',
        ));
        $fieldDescription->setAssociationAdmin($this->getMediaAdmin());
        $fieldDescription->setAdmin($formMapper->getAdmin());
        $fieldDescription->setOption('edit', 'list');
        $fieldDescription->setAssociationMapping(array(
            'fieldName' => 'media',
            'type' => ClassMetadataInfo::MANY_TO_ONE,
        ));

        // NEXT_MAJOR: Keep FQCN when bumping Symfony requirement to 2.8+.
        $modelListType = method_exists('Symfony\Component\Form\AbstractType', 'getBlockPrefix')
            ? 'Sonata\AdminBundle\Form\Type\ModelListType'
            : 'sonata_type_model_list';

        $fieldOptions = array_merge(
        	array(
        		'sonata_field_description' => $fieldDescription,
	            'class' => $this->getMediaAdmin()->getClass(),
	            'model_manager' => $this->getMediaAdmin()->getModelManager()
	        )
    	, $options);

        return $formMapper->create($fieldName, $modelListType, $fieldOptions);
    }

    public static function getLayoutOptions()
    {
    	return array(
    		'default' => 'Default',
    		'layout1' => 'Layout 1 (Has Bottom Content)',
    	);
    }

    public static function getLayoutTemplates()
    {
    	return array(
    		'default' => 'AppBundle:Block:background_content_layouts/default.html.twig',
    		'layout1' => 'AppBundle:Block:background_content_layouts/layout1.html.twig',
    	);
    }
}