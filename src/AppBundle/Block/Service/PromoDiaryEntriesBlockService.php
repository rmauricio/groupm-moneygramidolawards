<?php

namespace AppBundle\Block\Service;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use Sonata\BlockBundle\Model\BlockInterface;
use Sonata\BlockBundle\Block\BlockContextInterface;

use Sonata\AdminBundle\Form\FormMapper;
use Sonata\CoreBundle\Validator\ErrorElement;
use Sonata\BlockBundle\Block\Service\AbstractAdminBlockService;
use Sonata\CoreBundle\Model\Metadata;
use Symfony\Component\Form\FormBuilderInterface;

use Doctrine\ORM\Mapping\ClassMetadataInfo;
use Sonata\CoreBundle\Model\ManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Templating\EngineInterface;
use Sonata\MediaBundle\Model\MediaInterface;
use Sonata\MediaBundle\Provider\Pool;
use Sonata\FormatterBundle\Form\Type\FormatterType;
use Sonata\FormatterBundle\Form\Type\SimpleFormatterType;
use AppBundle\Entity\Promo\EntryInterface;
use AppBundle\Entity\Promo\Promo;
use Symfony\Component\Form\CallbackTransformer;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @author Ryan Mauricio <ryanmarkrmauricio@gmail.com>
 */
class PromoDiaryEntriesBlockService extends AbstractAdminBlockService 
{
	/**
     * @var ManagerInterface
     */
    protected $promoAdmin;

    /**
     * @var ManagerInterface
     */
    protected $promoManager;

	/**
     * @var ManagerInterface
     */
    protected $categoryAdmin;

    /**
     * @var ManagerInterface
     */
    protected $categoryManager;

    /**
     * @var ManagerInterface
     */
    protected $diaryEntriesManager;

    protected $requestStack;

    protected $blockUniqueID;

    /**
     * @param string             $name
     * @param EngineInterface    $templating
     * @param ManagerInterface   $mediaManager
     */
    public function __construct($name, EngineInterface $templating, $promoAdmin, $categoryAdmin, ManagerInterface $promoManager, ManagerInterface $categoryManager, ManagerInterface $diaryEntriesManager, $requestStack)
    {
        parent::__construct($name, $templating);

        $this->promoAdmin = $promoAdmin;
        $this->promoManager = $promoManager;
        $this->categoryAdmin = $categoryAdmin;
        $this->categoryManager = $categoryManager;
        $this->diaryEntriesManager = $diaryEntriesManager;
        $this->requestStack = $requestStack;
    }

	/**
     * {@inheritdoc}
     */
    public function execute(BlockContextInterface $blockContext, Response $response = null)
    {
    	$settings = $blockContext->getSettings();

        $promos = array();
        $templateLayouts = self::getLayoutTemplates();
        $templateLayout = $templateLayouts[$settings['layout']];

        $promoFilter = null;
        if (isset($settings['promo'])) {
            $promoFilter = $this->promoManager->find($settings['promo']);
        }

        $limit = null;
        if (!empty($settings['limit'])) {
            $limit = $settings['limit'];
        }

        $entries = $this->diaryEntriesManager->findByCategories($promoFilter, $settings['categories'], $limit);

        if ($settings['showPromoDropdown']) {
            $promos = $this->promoManager->findBy(array('enabled' => true, 'displayWinners' => true, 'type' => Promo::TYPE_DIARY), array('publishStart' => 'DESC', 'optionDisplayName' => 'ASC'));
        }

        $filters = array(
            'promo' => $promoFilter
        );

        return $this->renderResponse($blockContext->getTemplate(), array(
            'block' => $blockContext->getBlock(),
            'settings' => $settings,
            'promos' => $promos,
            'entries' => $entries,
            'templateLayout' => $templateLayout,
            'filters' => $filters,
        ), $response);
    }

    /**
     * {@inheritdoc}
     */
    public function buildEditForm(FormMapper $formMapper, BlockInterface $block)
    {
    	if (!$block->getSetting('promo') instanceof Promo) {
            $this->load($block);
        }

        // NEXT_MAJOR: Keep FQCN when bumping Symfony requirement to 2.8+.
        if (method_exists('Symfony\Component\Form\AbstractType', 'getBlockPrefix')) {
            $immutableArrayType = 'Sonata\CoreBundle\Form\Type\ImmutableArrayType';
            $textType = 'Symfony\Component\Form\Extension\Core\Type\TextType';
            $integerType = 'Symfony\Component\Form\Extension\Core\Type\IntegerType';
            $choiceType = 'Symfony\Component\Form\Extension\Core\Type\ChoiceType';
            $checkboxType = 'Symfony\Component\Form\Extension\Core\Type\CheckboxType';
        } else {
            $immutableArrayType = 'sonata_type_immutable_array';
            $textType = 'text';
            $integerType = 'integer';
            $checkboxType = 'checkbox';
            $choiceType = 'choice';
        }

        $formMapper->add('settings', 'sonata_type_immutable_array', array(
            'keys' => array(
                /*array(
                    'title',
                    $textType,
                    array(
                        'required' => false,
                        'label' => 'Title',
                    )
                ),*/
                array($this->getPromoBuilder('promo', $formMapper, array('label' => 'Promo', 'required' => false, 'btn_edit' => false)), null, array()),
                array($this->getCategoryBuilder('category', $formMapper, array('label' => 'Category', 'required' => false, 'btn_edit' => false)), null, array()),
                array($this->getCategoryBuilder('categories', $formMapper, array('label' => 'Categories', 'required' => false), true), null, array()),
                array(
                    'limit',
                    $integerType,
                    array(
                        'required' => false,
                        'label' => 'Limit',
                    )
                ),
                /*array(
                    'enablePager',
                    $checkboxType,
                    array(
                        'required' => false,
                        'label' => 'Enable Pager',
                    )
                ),*/
                array(
                    'layout', $choiceType, array(
	                    'required' => true,
	                    'choices' => array_flip(self::getLayoutOptions()),
	                    'label' => 'Layout',
	                )
                ),
                array(
                    'class',
                    $textType,
                    array(
                        'required' => false,
                        'label' => 'Class',
                    )
                ),
                array(
                    'headerTitle',
                    $textType,
                    array(
                        'required' => false,
                        'label' => 'Header Title',
                    )
                ),
                array(
                    'headerTitleClass',
                    $textType,
                    array(
                        'required' => false,
                        'label' => 'Header Title Class',
                    )
                ),
                array('footerContent', SimpleFormatterType::class, array(
                    'format' => 'richhtml',
                    'ckeditor_context' => 'default'
                )),
                array(
                    'showCategory',
                    $checkboxType,
                    array(
                        'required' => false,
                        'label' => 'Show Category',
                    )
                ),
                array(
                    'showPromoDropdown',
                    $checkboxType,
                    array(
                        'required' => false,
                        'label' => 'Show Promo Dropdown',
                    )
                ),
                array(
                    'promoDropDownFieldLabel',
                    $textType,
                    array(
                        'required' => false,
                        'label' => 'Promo Dropdown Field Label',
                    )
                ),
                array(
                    'showNoResultsMessage',
                    $checkboxType,
                    array(
                        'required' => false,
                        'label' => 'Show No Results Message',
                    )
                )
            ),
        ));

        $categoryViewCallbackTransformer = $this->categoryViewCallbackTransformer();
        $formMapper->get('settings')->get('categories')->addViewTransformer($categoryViewCallbackTransformer);

        if (($blockUniqueID = $formMapper->getAdmin()->getUniqid())) {
            $this->blockUniqueId = $blockUniqueID;
        }

        parent::buildEditForm($formMapper, $block);
    }

    /**
     * @return CallbackTransformer
     */
    protected function categoryViewCallbackTransformer()
    {
        return new CallbackTransformer(
            function ($categories) {
                return $categories;
            },
            function ($categories) {
                if (is_array($categories)) {
                    $categories = array_filter($categories, function($val){
                        if (empty($val)) {
                            return false;
                        }
                        return true;
                    });
                    return $categories;
                }
                return $categories;
            }
        );
    }

    /**
     * {@inheritdoc}
     */
    public function configureSettings(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'layout' => 'default',
            'promo' => null,
            'category' => null,
            'categories' => null,
            'limit' => null,
            'enablePager' => false,
            'headerTitle' => null,
            'headerTitleClass' => null,
            'class' => null,
            'footerContent' => null,
            'showCategory' => false,
            'showPromoDropdown' => false,
            'promoDropDownFieldLabel' => null,
            'showNoResultsMessage' => false,
            'template' => 'AppBundle:Block:Promo/diary_entries.html.twig',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function load(BlockInterface $block)
    {
        $promo = $block->getSetting('promo', null);

        if (is_int($promo)) {
            $promo = $this->promoManager->findOneBy(array('id' => $promo));
        }

        $block->setSetting('promo', $promo);

        $category = $block->getSetting('category', null);

        if (is_int($category)) {
            $category = $this->categoryManager->findOneBy(array('id' => $category));
        }

        $block->setSetting('category', $category);

        $this->initCategories($block, 'categories');
    }

    /**
     * Generic collections initialization
     * 
     * @param BlockInterface $block
     * @param string $fieldNames
     */
    protected function initCategories(BlockInterface $block, $fieldName = '')
    {
        $categoryArray = $block->getSetting($fieldName, array());
        
        $categoryArrayCollection = new ArrayCollection();
        
        if (!empty($categoryArray)) {
            if ($categoryArray && count($categoryArray)) {
                foreach($categoryArray as $categoryId) {
                    if (($category = $this->categoryManager->findOneBy(array('id' => $categoryId)))) {
                        $categoryArrayCollection->add($category);
                    }
                }
            }
        }
        $block->setSetting($fieldName, $categoryArrayCollection);
    }

    /**
     * {@inheritdoc}
     */
    public function prePersist(BlockInterface $block)
    {
        $block->setSetting('promo', is_object($block->getSetting('promo')) ? $block->getSetting('promo')->getId() : null);
        $block->setSetting('category', is_object($block->getSetting('category')) ? $block->getSetting('category')->getId() : null);

        if (($categories = $block->getSetting('categories')) && ($categories instanceof ArrayCollection && $categories->count() > 0)) {
            $block->setSetting('categories', $this->getPostedCategories('categories'));
        } else {
            $block->setSetting('categories', null);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function preUpdate(BlockInterface $block)
    {
        $block->setSetting('promo', is_object($block->getSetting('promo')) ? $block->getSetting('promo')->getId() : null);
        $block->setSetting('category', is_object($block->getSetting('category')) ? $block->getSetting('category')->getId() : null);

        if (($categories = $block->getSetting('categories')) && ($categories instanceof ArrayCollection && $categories->count() > 0)) {
            $block->setSetting('categories', $this->getPostedCategories('categories'));
        } else {
            $block->setSetting('categories', null);
        }
    }

    /**
     * Get Posted collections in order to preserved the collection order
     * 
     * @param type $fieldName
     * @return array
     */
    protected function getPostedCategories($fieldName)
    {
        if (empty($this->blockUniqueId)) {
            return null;
        }
        
        if (($requestStack = $this->requestStack)) {
            $currentRequest = $requestStack->getCurrentRequest();
            if (($blockInfo = $currentRequest->request->get($this->blockUniqueId))) {
                if (isset($blockInfo['settings']) && isset($blockInfo['settings'][$fieldName])) {
                    // just to make sure values are returned as integer
                    return array_map(function($value){
                        return (int) $value;
                    }, $blockInfo['settings'][$fieldName]);
                }
            }
        }
        return null;
    }

    public function getBlockMetadata($code = null): Metadata
    {
        return new Metadata(
            $this->getName(),
            null !== $code ? $code : $this->getName(),
            false,
            'AppBundle',
            ['class' => 'fa fa-vcard']
        );
    }

    /**
     * @param FormMapper $formMapper
     *
     * @return FormBuilder
     */
    protected function getPromoBuilder($fieldName, FormMapper $formMapper, $options = array())
    {
        // simulate an association ...
        $fieldDescription = $this->promoAdmin->getModelManager()->getNewFieldDescriptionInstance($this->promoAdmin->getClass(), 'promo', array());
        $fieldDescription->setAssociationAdmin($this->promoAdmin);
        $fieldDescription->setAdmin($formMapper->getAdmin());
        $fieldDescription->setOption('edit', 'list');
        $fieldDescription->setAssociationMapping(array(
            'fieldName' => 'promo',
            'type' => ClassMetadataInfo::MANY_TO_ONE,
        ));

        // NEXT_MAJOR: Keep FQCN when bumping Symfony requirement to 2.8+.
        $modelListType = method_exists('Symfony\Component\Form\AbstractType', 'getBlockPrefix')
            ? 'Sonata\AdminBundle\Form\Type\ModelListType'
            : 'sonata_type_model_list';

        $fieldOptions = array_merge(
        	array(
        		'sonata_field_description' => $fieldDescription,
	            'class' => $this->promoAdmin->getClass(),
	            'model_manager' => $this->promoAdmin->getModelManager(),
	        )
    	, $options);


        return $formMapper->create($fieldName, $modelListType, $fieldOptions);
    }

    /**
     * @param FormMapper $formMapper
     *
     * @return FormBuilder
     */
    protected function getCategoryBuilder($fieldName, FormMapper $formMapper, $options = array(), $multiple = false, $label = null, $help = null)
    {
        // simulate an association ...
        $fieldDescription = $this->categoryAdmin->getModelManager()->getNewFieldDescriptionInstance($this->categoryAdmin->getClass(), $fieldName, array());
        $fieldDescription->setAssociationAdmin($this->categoryAdmin);
        $fieldDescription->setAdmin($formMapper->getAdmin());
        if ($multiple) {
            $fieldDescription->setOption('edit', 'standard');
            $fieldDescription->setAssociationMapping(array(
                'fieldName' => $fieldName,
                'type'      => ClassMetadataInfo::MANY_TO_MANY
            ));

            $entityManager = $this->categoryAdmin->getModelManager()->getEntityManager('Application\Sonata\ClassificationBundle\Entity\Category');

            $query = $entityManager->createQueryBuilder('c')
                        ->select('c')
                        ->from('ApplicationSonataClassificationBundle:Category', 'c')
                        ->where("c.context = 'promo'")
                        ->orderBy('c.name', 'ASC');
            
            return $formMapper->create($fieldName, 'sonata_type_model', array(
                'sonata_field_description' => $fieldDescription,
                'class'                    => $this->categoryAdmin->getClass(),
                'model_manager'            => $this->categoryAdmin->getModelManager(),
                'label'                    => $label,
                'sonata_help'              => $help,
                'property'                 => 'name',
                'multiple'                 => true,
                'required'                 => false,
                'expanded'                 => false,
                'sortable'                 => true,
                'btn_add'                  => false,
                'query'                    => $query,
                'attr'                     => array('data-sonata-select2' => false)
            ));
        } else {
            $fieldDescription->setOption('edit', 'list');
            $fieldDescription->setAssociationMapping(array(
                'fieldName' => 'category',
                'type' => ClassMetadataInfo::MANY_TO_ONE,
            ));
            // NEXT_MAJOR: Keep FQCN when bumping Symfony requirement to 2.8+.
            $modelListType = method_exists('Symfony\Component\Form\AbstractType', 'getBlockPrefix')
                ? 'Sonata\AdminBundle\Form\Type\ModelListType'
                : 'sonata_type_model_list';

            $fieldDescription->setOption('link_parameters', array('context' => 'promo', 'hide_context' => true));

            $fieldOptions = array_merge(
                array(
                    'sonata_field_description' => $fieldDescription,
                    'class' => $this->categoryAdmin->getClass(),
                    'model_manager' => $this->categoryAdmin->getModelManager()
                )
            , $options);

            return $formMapper->create($fieldName, $modelListType, $fieldOptions);
        }
    }

    public static function getLayoutOptions()
    {
        return array(
            'default' => 'Default',
            'layout1' => 'Layout 1',
            'layout2' => 'Layout 2',
        );
    }

    public static function getLayoutTemplates()
    {
    	return array(
    		'default' => 'AppBundle:Block:Promo/diary_entries_layouts/default.html.twig',
    		'layout1' => 'AppBundle:Block:Promo/diary_entries_layouts/layout1.html.twig',
    		'layout2' => 'AppBundle:Block:Promo/diary_entries_layouts/layout2.html.twig',
    	);
    }
}