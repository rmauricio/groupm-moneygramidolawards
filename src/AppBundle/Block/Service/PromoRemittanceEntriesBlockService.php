<?php

namespace AppBundle\Block\Service;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use Sonata\BlockBundle\Model\BlockInterface;
use Sonata\BlockBundle\Block\BlockContextInterface;

use Sonata\AdminBundle\Form\FormMapper;
use Sonata\CoreBundle\Validator\ErrorElement;
use Sonata\BlockBundle\Block\Service\AbstractAdminBlockService;
use Sonata\CoreBundle\Model\Metadata;
use Symfony\Component\Form\FormBuilderInterface;

use Doctrine\ORM\Mapping\ClassMetadataInfo;
use Sonata\CoreBundle\Model\ManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Templating\EngineInterface;
use Sonata\MediaBundle\Model\MediaInterface;
use Sonata\MediaBundle\Provider\Pool;
use Sonata\FormatterBundle\Form\Type\FormatterType;
use Sonata\FormatterBundle\Form\Type\SimpleFormatterType;
use AppBundle\Entity\Promo\EntryInterface;
use AppBundle\Entity\Promo\Promo;

/**
 * @author Ryan Mauricio <ryanmarkrmauricio@gmail.com>
 */
class PromoRemittanceEntriesBlockService extends AbstractAdminBlockService 
{
	/**
     * @var ManagerInterface
     */
    protected $promoAdmin;

    /**
     * @var ManagerInterface
     */
    protected $promoManager;

	/**
     * @var ManagerInterface
     */
    protected $categoryAdmin;

    /**
     * @var ManagerInterface
     */
    protected $categoryManager;

    /**
     * @var ManagerInterface
     */
    protected $remittanceEntriesManager;

    /**
     * @param string             $name
     * @param EngineInterface    $templating
     * @param ManagerInterface   $mediaManager
     */
    public function __construct($name, EngineInterface $templating, $promoAdmin, $categoryAdmin, ManagerInterface $promoManager, ManagerInterface $categoryManager, ManagerInterface $remittanceEntriesManager)
    {
        parent::__construct($name, $templating);

        $this->promoAdmin = $promoAdmin;
        $this->promoManager = $promoManager;
        $this->categoryAdmin = $categoryAdmin;
        $this->categoryManager = $categoryManager;
        $this->remittanceEntriesManager = $remittanceEntriesManager;
    }

	/**
     * {@inheritdoc}
     */
    public function execute(BlockContextInterface $blockContext, Response $response = null)
    {
    	$settings = $blockContext->getSettings();

        $promos = array();
        $templateLayouts = self::getLayoutTemplates();
        $templateLayout = $templateLayouts[$settings['layout']];

        $filters = array('status' => EntryInterface::STATUS_PUBLISHED);
        if (isset($settings['promo'])) {
            $promoFilter = $this->promoManager->find($settings['promo']);
            if ($promoFilter) {
                $filters['promo'] = $promoFilter;
            }
        }
        if (isset($settings['category'])) {
            $categoryFilter = $this->categoryManager->find($settings['category']);
            if ($categoryFilter) {
                $filters['category'] = $categoryFilter;
            }
        }

        $limit = null;
        if (!empty($settings['limit'])) {
            $limit = $settings['limit'];
        }

        $entries = $this->remittanceEntriesManager->findBy($filters, array(
            'position' => 'ASC',
            'title' => 'ASC',
        ), $limit);

        if ($settings['showPromoDropdown']) {
            $promos = $this->promoManager->findBy(array('enabled' => true, 'displayWinners' => true, 'type' => Promo::TYPE_REMITTANCE), array('publishStart' => 'DESC', 'optionDisplayName' => 'ASC'));
        }

        return $this->renderResponse($blockContext->getTemplate(), array(
            'block' => $blockContext->getBlock(),
            'settings' => $settings,
            'promos' => $promos,
            'entries' => $entries,
            'templateLayout' => $templateLayout,
            'filters' => $filters,
        ), $response);
    }

    /**
     * {@inheritdoc}
     */
    public function buildEditForm(FormMapper $formMapper, BlockInterface $block)
    {
    	if (!$block->getSetting('promo') instanceof Promo) {
            $this->load($block);
        }

        // NEXT_MAJOR: Keep FQCN when bumping Symfony requirement to 2.8+.
        if (method_exists('Symfony\Component\Form\AbstractType', 'getBlockPrefix')) {
            $immutableArrayType = 'Sonata\CoreBundle\Form\Type\ImmutableArrayType';
            $textType = 'Symfony\Component\Form\Extension\Core\Type\TextType';
            $integerType = 'Symfony\Component\Form\Extension\Core\Type\IntegerType';
            $choiceType = 'Symfony\Component\Form\Extension\Core\Type\ChoiceType';
            $checkboxType = 'Symfony\Component\Form\Extension\Core\Type\CheckboxType';
        } else {
            $immutableArrayType = 'sonata_type_immutable_array';
            $textType = 'text';
            $integerType = 'integer';
            $checkboxType = 'checkbox';
            $choiceType = 'choice';
        }

        $formMapper->add('settings', 'sonata_type_immutable_array', array(
            'keys' => array(
                /*array(
                    'title',
                    $textType,
                    array(
                        'required' => false,
                        'label' => 'Title',
                    )
                ),*/
                array($this->getPromoBuilder('promo', $formMapper, array('label' => 'Promo', 'required' => false, 'btn_edit' => false)), null, array()),
                array($this->getCategoryBuilder('category', $formMapper, array('label' => 'Category', 'required' => false, 'btn_edit' => false)), null, array()),
                array(
                    'limit',
                    $integerType,
                    array(
                        'required' => false,
                        'label' => 'Limit',
                    )
                ),
                /*array(
                    'enablePager',
                    $checkboxType,
                    array(
                        'required' => false,
                        'label' => 'Enable Pager',
                    )
                ),*/
                array(
                    'layout', $choiceType, array(
	                    'required' => true,
	                    'choices' => array_flip(self::getLayoutOptions()),
	                    'label' => 'Layout',
	                )
                ),
                array(
                    'class',
                    $textType,
                    array(
                        'required' => false,
                        'label' => 'Class',
                    )
                ),
                array(
                    'headerTitle',
                    $textType,
                    array(
                        'required' => false,
                        'label' => 'Header Title',
                    )
                ),
                array(
                    'headerTitleClass',
                    $textType,
                    array(
                        'required' => false,
                        'label' => 'Header Title Class',
                    )
                ),
                array('footerContent', SimpleFormatterType::class, array(
                    'format' => 'richhtml',
                    'ckeditor_context' => 'default'
                )),
                array(
                    'showCategory',
                    $checkboxType,
                    array(
                        'required' => false,
                        'label' => 'Show Category',
                    )
                ),
                array(
                    'showPromoDropdown',
                    $checkboxType,
                    array(
                        'required' => false,
                        'label' => 'Show Promo Dropdown',
                    )
                ),
                array(
                    'promoDropDownFieldLabel',
                    $textType,
                    array(
                        'required' => false,
                        'label' => 'Promo Dropdown Field Label',
                    )
                ),
                array(
                    'showNoResultsMessage',
                    $checkboxType,
                    array(
                        'required' => false,
                        'label' => 'Show No Results Message',
                    )
                )
            ),
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function configureSettings(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'layout' => 'default',
            'promo' => null,
            'category' => null,
            'limit' => null,
            'enablePager' => false,
            'headerTitle' => null,
            'headerTitleClass' => null,
            'class' => null,
            'footerContent' => null,
            'showCategory' => false,
            'showPromoDropdown' => false,
            'promoDropDownFieldLabel' => null,
            'showNoResultsMessage' => false,
            'template' => 'AppBundle:Block:Promo/remittance_entries.html.twig',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function load(BlockInterface $block)
    {
        $promo = $block->getSetting('promo', null);

        if (is_int($promo)) {
            $promo = $this->promoManager->findOneBy(array('id' => $promo));
        }

        $block->setSetting('promo', $promo);

        $category = $block->getSetting('category', null);

        if (is_int($category)) {
            $category = $this->categoryManager->findOneBy(array('id' => $category));
        }

        $block->setSetting('category', $category);
    }

    /**
     * {@inheritdoc}
     */
    public function prePersist(BlockInterface $block)
    {
        $block->setSetting('promo', is_object($block->getSetting('promo')) ? $block->getSetting('promo')->getId() : null);
        $block->setSetting('category', is_object($block->getSetting('category')) ? $block->getSetting('category')->getId() : null);
    }

    /**
     * {@inheritdoc}
     */
    public function preUpdate(BlockInterface $block)
    {
        $block->setSetting('promo', is_object($block->getSetting('promo')) ? $block->getSetting('promo')->getId() : null);
        $block->setSetting('category', is_object($block->getSetting('category')) ? $block->getSetting('category')->getId() : null);
    }

    public function getBlockMetadata($code = null): Metadata
    {
        return new Metadata(
            $this->getName(),
            null !== $code ? $code : $this->getName(),
            false,
            'AppBundle',
            ['class' => 'fa fa-vcard']
        );
    }

    /**
     * @param FormMapper $formMapper
     *
     * @return FormBuilder
     */
    protected function getPromoBuilder($fieldName, FormMapper $formMapper, $options = array())
    {
        // simulate an association ...
        $fieldDescription = $this->promoAdmin->getModelManager()->getNewFieldDescriptionInstance($this->promoAdmin->getClass(), 'promo', array());
        $fieldDescription->setAssociationAdmin($this->promoAdmin);
        $fieldDescription->setAdmin($formMapper->getAdmin());
        $fieldDescription->setOption('edit', 'list');
        $fieldDescription->setAssociationMapping(array(
            'fieldName' => 'promo',
            'type' => ClassMetadataInfo::MANY_TO_ONE,
        ));

        // NEXT_MAJOR: Keep FQCN when bumping Symfony requirement to 2.8+.
        $modelListType = method_exists('Symfony\Component\Form\AbstractType', 'getBlockPrefix')
            ? 'Sonata\AdminBundle\Form\Type\ModelListType'
            : 'sonata_type_model_list';

        $fieldOptions = array_merge(
        	array(
        		'sonata_field_description' => $fieldDescription,
	            'class' => $this->promoAdmin->getClass(),
	            'model_manager' => $this->promoAdmin->getModelManager(),
	        )
    	, $options);


        return $formMapper->create($fieldName, $modelListType, $fieldOptions);
    }

    /**
     * @param FormMapper $formMapper
     *
     * @return FormBuilder
     */
    protected function getCategoryBuilder($fieldName, FormMapper $formMapper, $options = array())
    {
        // simulate an association ...
        $fieldDescription = $this->categoryAdmin->getModelManager()->getNewFieldDescriptionInstance($this->categoryAdmin->getClass(), 'category', array());
        $fieldDescription->setAssociationAdmin($this->categoryAdmin);
        $fieldDescription->setAdmin($formMapper->getAdmin());
        $fieldDescription->setOption('edit', 'list');
        $fieldDescription->setAssociationMapping(array(
            'fieldName' => 'category',
            'type' => ClassMetadataInfo::MANY_TO_ONE,
        ));

        // NEXT_MAJOR: Keep FQCN when bumping Symfony requirement to 2.8+.
        $modelListType = method_exists('Symfony\Component\Form\AbstractType', 'getBlockPrefix')
            ? 'Sonata\AdminBundle\Form\Type\ModelListType'
            : 'sonata_type_model_list';

        $fieldDescription->setOption('link_parameters', array('context' => 'promo', 'hide_context' => true));

        $fieldOptions = array_merge(
        	array(
        		'sonata_field_description' => $fieldDescription,
	            'class' => $this->categoryAdmin->getClass(),
	            'model_manager' => $this->categoryAdmin->getModelManager()
	        )
    	, $options);

        return $formMapper->create($fieldName, $modelListType, $fieldOptions);
    }

    public static function getLayoutOptions()
    {
        return array(
            'default' => 'Default',
            'layout1' => 'Layout 1',
            'layout2' => 'Layout 2',
        );
    }

    public static function getLayoutTemplates()
    {
    	return array(
    		'default' => 'AppBundle:Block:Promo/remittance_entries_layouts/default.html.twig',
    		'layout1' => 'AppBundle:Block:Promo/remittance_entries_layouts/layout1.html.twig',
    		'layout2' => 'AppBundle:Block:Promo/remittance_entries_layouts/layout2.html.twig',
    	);
    }
}