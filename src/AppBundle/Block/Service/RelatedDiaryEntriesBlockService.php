<?php

namespace AppBundle\Block\Service;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use Sonata\BlockBundle\Model\BlockInterface;
use Sonata\BlockBundle\Block\BlockContextInterface;

use Sonata\AdminBundle\Form\FormMapper;
use Sonata\CoreBundle\Validator\ErrorElement;
use Sonata\BlockBundle\Block\Service\AbstractAdminBlockService;
use Sonata\CoreBundle\Model\Metadata;
use Symfony\Component\Form\FormBuilderInterface;

use Doctrine\ORM\Mapping\ClassMetadataInfo;
use Sonata\CoreBundle\Model\ManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Templating\EngineInterface;
use Sonata\MediaBundle\Model\MediaInterface;
use Sonata\MediaBundle\Provider\Pool;
use Sonata\FormatterBundle\Form\Type\FormatterType;
use Sonata\FormatterBundle\Form\Type\SimpleFormatterType;
use AppBundle\Entity\Promo\EntryInterface;
use AppBundle\Entity\Promo\Promo;

/**
 * @author Ryan Mauricio <ryanmarkrmauricio@gmail.com>
 */
class RelatedDiaryEntriesBlockService extends AbstractAdminBlockService 
{
	/**
     * @var ManagerInterface
     */
    protected $diaryEntryAdmin;

    /**
     * @var ManagerInterface
     */
    protected $diaryEntryManager;

    /**
     * @param string             $name
     * @param EngineInterface    $templating
     * @param ManagerInterface   $mediaManager
     */
    public function __construct($name, EngineInterface $templating, $diaryEntryAdmin, ManagerInterface $diaryEntryManager)
    {
        parent::__construct($name, $templating);

        $this->diaryEntryAdmin = $diaryEntryAdmin;
        $this->diaryEntryManager = $diaryEntryManager;
    }

	/**
     * {@inheritdoc}
     */
    public function execute(BlockContextInterface $blockContext, Response $response = null)
    {
    	$settings = $blockContext->getSettings();

        $promos = array();
        $templateLayouts = self::getLayoutTemplates();
        $templateLayout = $templateLayouts[$settings['layout']];

        $limit = null;
        if (!empty($settings['limit'])) {
            $limit = $settings['limit'];
        }

        $entry = null;
        if (isset($settings['entry'])) {
            $entry = $this->diaryEntryManager->find($settings['entry']);
        }

        $entries = $this->diaryEntryManager->findRelated($entry, $limit);

        return $this->renderResponse($blockContext->getTemplate(), array(
            'block' => $blockContext->getBlock(),
            'settings' => $settings,
            'entries' => $entries,
            'templateLayout' => $templateLayout,
        ), $response);
    }

    /**
     * {@inheritdoc}
     */
    public function buildEditForm(FormMapper $formMapper, BlockInterface $block)
    {
    	if (!$block->getSetting('promo') instanceof Promo) {
            $this->load($block);
        }

        // NEXT_MAJOR: Keep FQCN when bumping Symfony requirement to 2.8+.
        if (method_exists('Symfony\Component\Form\AbstractType', 'getBlockPrefix')) {
            $immutableArrayType = 'Sonata\CoreBundle\Form\Type\ImmutableArrayType';
            $textType = 'Symfony\Component\Form\Extension\Core\Type\TextType';
            $integerType = 'Symfony\Component\Form\Extension\Core\Type\IntegerType';
            $choiceType = 'Symfony\Component\Form\Extension\Core\Type\ChoiceType';
            $checkboxType = 'Symfony\Component\Form\Extension\Core\Type\CheckboxType';
        } else {
            $immutableArrayType = 'sonata_type_immutable_array';
            $textType = 'text';
            $integerType = 'integer';
            $checkboxType = 'checkbox';
            $choiceType = 'choice';
        }

        $formMapper->add('settings', 'sonata_type_immutable_array', array(
            'keys' => array(
                array($this->getDiaryEntryBuilder('entry', $formMapper, array('label' => 'Diary Entry', 'required' => false, 'btn_edit' => false)), null, array()),
                array(
                    'limit',
                    $integerType,
                    array(
                        'required' => false,
                        'label' => 'Limit',
                    )
                ),
                array(
                    'layout', $choiceType, array(
	                    'required' => true,
	                    'choices' => array_flip(self::getLayoutOptions()),
	                    'label' => 'Layout',
	                )
                ),
                array(
                    'class',
                    $textType,
                    array(
                        'required' => false,
                        'label' => 'Class',
                    )
                ),
                array(
                    'headerTitle',
                    $textType,
                    array(
                        'required' => false,
                        'label' => 'Header Title',
                    )
                ),
                array(
                    'headerTitleClass',
                    $textType,
                    array(
                        'required' => false,
                        'label' => 'Header Title Class',
                    )
                ),
            ),
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function configureSettings(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'layout' => 'default',
            'entry' => null,
            'limit' => null,
            'headerTitle' => null,
            'headerTitleClass' => null,
            'class' => null,
            'template' => 'AppBundle:Block:Promo/related_diary_entries.html.twig',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function load(BlockInterface $block)
    {
        $entry = $block->getSetting('entry', null);

        if (is_int($entry)) {
            $entry = $this->diaryEntryManager->findOneBy(array('id' => $entry));
        }

        $block->setSetting('entry', $entry);
    }

    /**
     * {@inheritdoc}
     */
    public function prePersist(BlockInterface $block)
    {
        $block->setSetting('entry', is_object($block->getSetting('entry')) ? $block->getSetting('entry')->getId() : null);
    }

    /**
     * {@inheritdoc}
     */
    public function preUpdate(BlockInterface $block)
    {
        $block->setSetting('entry', is_object($block->getSetting('entry')) ? $block->getSetting('entry')->getId() : null);
    }

    public function getBlockMetadata($code = null): Metadata
    {
        return new Metadata(
            $this->getName(),
            null !== $code ? $code : $this->getName(),
            false,
            'AppBundle',
            ['class' => 'fa fa-vcard']
        );
    }

    /**
     * @param FormMapper $formMapper
     *
     * @return FormBuilder
     */
    protected function getDiaryEntryBuilder($fieldName, FormMapper $formMapper, $options = array())
    {
        // simulate an association ...
        $fieldDescription = $this->diaryEntryAdmin->getModelManager()->getNewFieldDescriptionInstance($this->diaryEntryAdmin->getClass(), 'entry', array());
        $fieldDescription->setAssociationAdmin($this->diaryEntryAdmin);
        $fieldDescription->setAdmin($formMapper->getAdmin());
        $fieldDescription->setOption('edit', 'list');
        $fieldDescription->setAssociationMapping(array(
            'fieldName' => 'entry',
            'type' => ClassMetadataInfo::MANY_TO_ONE,
        ));

        // NEXT_MAJOR: Keep FQCN when bumping Symfony requirement to 2.8+.
        $modelListType = method_exists('Symfony\Component\Form\AbstractType', 'getBlockPrefix')
            ? 'Sonata\AdminBundle\Form\Type\ModelListType'
            : 'sonata_type_model_list';

        $fieldOptions = array_merge(
        	array(
        		'sonata_field_description' => $fieldDescription,
	            'class' => $this->diaryEntryAdmin->getClass(),
	            'model_manager' => $this->diaryEntryAdmin->getModelManager(),
	        )
    	, $options);


        return $formMapper->create($fieldName, $modelListType, $fieldOptions);
    }

    public static function getLayoutOptions()
    {
        return array(
            'default' => 'Default',
        );
    }

    public static function getLayoutTemplates()
    {
    	return array(
    		'default' => 'AppBundle:Block:Promo/related_diary_entries_layouts/default.html.twig',
    	);
    }
}