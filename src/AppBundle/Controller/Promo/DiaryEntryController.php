<?php

namespace AppBundle\Controller\Promo;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Form\DiaryEntryType;
use AppBundle\Entity\Promo\Promo;
use AppBundle\Entity\Promo\Entry;
use AppBundle\Entity\Promo\EntryInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use AppBundle\Block\Service\PromoRemittanceEntriesBlockService;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use EWZ\Bundle\RecaptchaBundle\Form\Type\EWZRecaptchaType;
use EWZ\Bundle\RecaptchaBundle\Validator\Constraints\IsTrue as RecaptchaTrue;

class DiaryEntryController extends Controller
{
    public function indexAction(Request $request)
    {
        $promo = $this->get('app.manager.promo.promo')->findActive(Promo::TYPE_DIARY);
        $formView = null;

        if ($promo) {
            $entry = $this->get('app.manager.promo.diary_entry')->create();
            $entry->setPromo($promo);

            $form = $this->_getDiaryEntryForm($entry);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $entry = $form->getData();

                $mediaManager = $this->get('sonata.media.manager.media');
                if ($entry->getImageFile()) {
                    $media = $mediaManager->create();
                    $media->setName($entry->getImageFile()->getClientOriginalName());
                    $media->setBinaryContent($entry->getImageFile());                        
                    $media->setContext('default');
                    $media->setProviderName('sonata.media.provider.image');
                    $mediaManager->save($media);
                    $entry->setImage($media);
                }
                if ($entry->getVideoFile()) {
                    $media = $mediaManager->create();
                    $media->setName($entry->getVideoFile()->getClientOriginalName());
                    $media->setBinaryContent($entry->getVideoFile());                        
                    $media->setContext('default');
                    $media->setProviderName('sonata.media.provider.file');
                    $mediaManager->save($media);
                    $entry->setVideo($media);
                }

                $this->get('app.manager.promo.diary_entry')->save($entry);

                $this->addFlash(
                    'lastSubmittedDiaryEntry',
                    $entry->getId()
                );
                return $this->redirectToRoute('app_promo_entry_diary_thankyou');
            }
            $formView = $form->createView();
        }

        return $this->render('AppBundle:Promo:entry_diary.html.twig', array(
            'form' => $formView,
            'promo' => $promo,
        ));
    }

    public function thankyouAction(Request $request, SessionInterface $session)
    {
        $lastSubmittedDiaryEntry = $session->getFlashBag()->get('lastSubmittedDiaryEntry', array());

        if (empty($lastSubmittedDiaryEntry)) {
            return $this->redirectToRoute('app_promo_entry_diary');
        } else {
            $lastSubmittedDiaryEntry = $this->get('app.manager.promo.diary_entry')->findOneBy(array('id'=> $lastSubmittedDiaryEntry[0]));
            if (!$lastSubmittedDiaryEntry) {
                throw $this->createNotFoundException('Entry not found.');
            }
        }

        return $this->render('AppBundle:Promo:entry_diary_thankyou.html.twig', array(
            'entry' => $lastSubmittedDiaryEntry,
        ));
    }

    public function blockListAction(Request $request)
    {
        $blockid = $request->attributes->get('block');
        $block = $this->get('sonata.page.manager.block')->findOneBy(array('id' => $blockid));

        $settings = array();
        $entries = array();
        $templateLayout = null;
        
        if ($block) {
            $settings = $block->getSettings();
            $templateLayouts = PromoRemittanceEntriesBlockService::getLayoutTemplates();
            $templateLayout = $templateLayouts[$settings['layout']];

            $filters = array('status' => EntryInterface::STATUS_PUBLISHED);
            if (isset($settings['promo'])) {
                $promoFilter = $this->get('app.manager.promo.promo')->find($settings['promo']);
                if ($promoFilter) {
                    $filters['promo'] = $promoFilter;
                }
            }
            if (isset($settings['category'])) {
                $categoryFilter = $this->get('sonata.classification.manager.category')->find($settings['category']);
                if ($categoryFilter) {
                    $filters['category'] = $categoryFilter;
                }
            }

            $entries = $this->get('app.manager.promo.diary_entry')->findBy($filters, array(
                'position' => 'ASC',
                'title' => 'ASC',
            ), $settings['limit'], 0);
        }

        return $this->render('AppBundle:Promo:entry_diary_list.html.twig', array(
            'block' => $block,
            'settings' => $settings,
            'entries' => $entries,
            'templateLayout' => $templateLayout,
        ));
    }

    public function finalistAction($slug)
    {
    	$promo = $this->get('app.manager.promo.promo')->findOneBy(array('slug' => $slug, 'enabled' => true));

        if (!$promo) {
            throw $this->createNotFoundException('Promo does not exist.');
        }

        $seoPage = $this->get('sonata.seo.page');
        $seoPage
            ->setTitle($promo->getName())
            ->addMeta('property', 'og:title', $promo->getName())
            ->addMeta('property', 'og:url',  $this->generateUrl('app_promo_diary_finalist', array('slug' => $slug), UrlGeneratorInterface::ABSOLUTE_URL))
        ;

        return $this->render('AppBundle:Promo:diary_finalist.html.twig', array(
            'promo' => $promo,
        ));
    }

    public function viewAction(Request $request, SessionInterface $session, $slug)
    {
    	$entry = $this->get('app.manager.promo.diary_entry')->findOneBy(array('slug' => $slug, 'status' => EntryInterface::STATUS_PUBLISHED));

        if (!$entry) {
            throw $this->createNotFoundException('Entry not found.');
        }

    	$voteSuccess = false;
        $votedDiaryEntry = $session->getFlashBag()->get('entryVoted', array());
        if (!empty($votedDiaryEntry) && $votedDiaryEntry[0] == $entry->getId()) {
        	$voteSuccess = true;
        }

    	$voted = false;
        $alreadyVoted = $session->getFlashBag()->get('alreadyVoted', array());
        if (!empty($alreadyVoted) && $alreadyVoted[0] == $entry->getId()) {
        	$voted = true;
        }

        $seoPage = $this->get('sonata.seo.page');
        $seoPage
            ->setTitle($entry->getTitle())
            ->addMeta('property', 'og:title', $entry->getTitle())
            ->addMeta('property', 'og:url',  $this->generateUrl('app_promo_diary_view', array('slug' => $slug), UrlGeneratorInterface::ABSOLUTE_URL))
        ;

        if ($entry->getThumbnail()) {
        	$provider = $this->get($entry->getThumbnail()->getProviderName());
            $seoPage->addMeta('property', 'og:image', $request->getUriForPath($provider->generatePublicUrl($entry->getThumbnail(), 'reference')));
        }

        return $this->render('AppBundle:Promo:diary_entry_view.html.twig', array(
            'entry' => $entry,
            'voteSuccess' => $voteSuccess,
            'voted' => $voted,
        ));
    }

    public function winnersAction(Request $request, $slug)
    {
        $promo = $this->get('app.manager.promo.promo')->findOneBy(array('slug' => $slug, 'enabled' => true));

        if (!$promo) {
            throw $this->createNotFoundException('Promo does not exist.');
        }

        $categoryWinner = $this->get('sonata.classification.manager.category')->findOneBy(array('slug' => 'diary-winner'));
        if (!$categoryWinner) {
            throw $this->createNotFoundException('Category winner does not exist.');
        }

        $categoriesWinner = $this->get('sonata.classification.manager.category')->findBy(array('parent' => $categoryWinner));
        $categories = array();
        foreach ($categoriesWinner as $category) {
            $categories[] = $category->getId();
        }

        $seoPage = $this->get('sonata.seo.page');
        $seoPage
            ->setTitle($promo->getName())
            ->addMeta('property', 'og:title', $promo->getName())
            ->addMeta('property', 'og:url',  $this->generateUrl('app_promo_remittance_winners', array('slug' => $slug), UrlGeneratorInterface::ABSOLUTE_URL))
        ;

        return $this->render('AppBundle:Promo:diary_winners.html.twig', array(
            'promo' => $promo,
            'categoryWinner' => $categoryWinner,
            'categoriesWinner' => $categoriesWinner,
            'categories' => $categories,
        ));
    }

    public function voteAction(Request $request, SessionInterface $session, $slug)
    {
    	$entry = $this->get('app.manager.promo.diary_entry')->findOneBy(array('slug' => $slug, 'status' => EntryInterface::STATUS_PUBLISHED));

        if (!$entry) {
            throw $this->createNotFoundException('Entry not found.');
        }

        $session->set('entryVote', $entry);

        return $this->redirectToRoute('hwi_oauth_service_redirect', array('service' => 'facebook', '_destination' => $this->generateUrl('app_promo_diary_view', array('slug' => $entry->getSlug()))));
    }

    private function _getDiaryEntryForm($entry, $validate = true)
    {
        $options = array();
        if (!$validate) {
            $options = array('validation_groups' => false);
        }

        $form = $this->createForm(DiaryEntryType::class, $entry, $options);
        $form
            ->add('agreeTerms', CheckboxType::class, array('label' => 'I agree'))
            ->add('recaptcha', EWZRecaptchaType::class, array(
                'attr' => array(
                    'options' => array(
                        'theme' => 'light',
                        'type'  => 'image',
                        'size'  => 'normal'
                    )
                ),
                'mapped'      => false,
                'constraints' => array(
                    new RecaptchaTrue()
                )
            ))
            ->add('submit', SubmitType::class, array('label' => 'Submit'))
        ;

        return $form;
    }
}
