<?php

namespace AppBundle\Controller\Promo;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Form\RemittanceEntryType;
use AppBundle\Form\DiaryEntryType;
use AppBundle\Entity\Promo\Entry;
use AppBundle\Entity\Promo\EntryInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use AppBundle\Block\Service\PromoRemittanceEntriesBlockService;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use EWZ\Bundle\RecaptchaBundle\Form\Type\EWZRecaptchaType;
use EWZ\Bundle\RecaptchaBundle\Validator\Constraints\IsTrue as RecaptchaTrue;

class EntryController extends Controller
{
    public function indexAction(Request $request)
    {
        $promo = $this->get('app.manager.promo.promo')->findActive();
        $formView = null;

        if ($promo) {
            $entry = $this->get('app.manager.promo.remittance_entry')->create();
            $entry->setPromo($promo);

            $form = $this->_getRemittanceEntryForm($entry);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $entry = $form->getData();

                $this->get('app.manager.promo.remittance_entry')->save($entry);

                $this->addFlash(
                    'lastSubmittedEntry',
                    $entry->getId()
                );
                return $this->redirectToRoute('app_promo_entry_thankyou');
            }
            $formView = $form->createView();
        }

        return $this->render('AppBundle:Promo:entry_remittance.html.twig', array(
            'form' => $formView,
            'promo' => $promo,
        ));
    }

    public function refreshRemittanceEntryFormAction(Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            return $this->redirectToRoute('app_promo_entry_remittance');
        }

        $promo = $this->get('app.manager.promo.promo')->findActive();

        $entry = $this->get('app.manager.promo.remittance_entry')->create();
        $entry->setPromo($promo);

        $form = $this->_getRemittanceEntryForm($entry, $request->request->get('validate'));
        $form->handleRequest($request);

        $formHtml = $this->renderView('AppBundle:Promo:entry_remittance_form.html.twig', array(
            'form' => $form->createView(),
        ));

        $response = new JsonResponse();
        $response->setData(array(
            'form_html' => $formHtml,
        ));

        return $response;
    }

    public function thankyouAction(Request $request, SessionInterface $session)
    {
        $lastSubmittedEntry = $session->getFlashBag()->get('lastSubmittedEntry', array());

        if (empty($lastSubmittedEntry)) {
            return $this->redirectToRoute('app_promo_entry_remittance');
        } else {
            $lastSubmittedEntry = $this->get('app.manager.promo.remittance_entry')->findOneBy(array('id'=> $lastSubmittedEntry[0]));
            if (!$lastSubmittedEntry) {
                throw $this->createNotFoundException('Entry not found.');
            }
        }

        return $this->render('AppBundle:Promo:entry_thankyou.html.twig', array(
            'entry' => $lastSubmittedEntry,
        ));
    }

    public function blockListAction(Request $request)
    {
        $blockid = $request->attributes->get('block');
        $block = $this->get('sonata.page.manager.block')->findOneBy(array('id' => $blockid));

        $settings = array();
        $entries = array();
        $templateLayout = null;
        
        if ($block) {
            $settings = $block->getSettings();
            $templateLayouts = PromoRemittanceEntriesBlockService::getLayoutTemplates();
            $templateLayout = $templateLayouts[$settings['layout']];

            $filters = array('status' => EntryInterface::STATUS_PUBLISHED);
            if (isset($settings['promo'])) {
                $promoFilter = $this->get('app.manager.promo.promo')->find($settings['promo']);
                if ($promoFilter) {
                    $filters['promo'] = $promoFilter;
                }
            }
            if (isset($settings['category'])) {
                $categoryFilter = $this->get('sonata.classification.manager.category')->find($settings['category']);
                if ($categoryFilter) {
                    $filters['category'] = $categoryFilter;
                }
            }

            $entries = $this->get('app.manager.promo.remittance_entry')->findBy($filters, array(
                'position' => 'ASC',
                'title' => 'ASC',
            ), $settings['limit'], 0);
        }

        return $this->render('AppBundle:Promo:entry_list.html.twig', array(
            'block' => $block,
            'settings' => $settings,
            'entries' => $entries,
            'templateLayout' => $templateLayout,
        ));
    }

    public function winnersAction(Request $request, $slug)
    {
        $promo = $this->get('app.manager.promo.promo')->findOneBy(array('slug' => $slug, 'enabled' => true));

        if (!$promo) {
            throw $this->createNotFoundException('Promo does not exist.');
        }

        $categoryWinner = $this->get('sonata.classification.manager.category')->findOneBy(array('slug' => 'winner'));
        if (!$categoryWinner) {
            throw $this->createNotFoundException('Category winner does not exist.');
        }

        $seoPage = $this->get('sonata.seo.page');
        $seoPage
            ->setTitle($promo->getName())
            ->addMeta('property', 'og:title', $promo->getName())
            ->addMeta('property', 'og:url',  $this->generateUrl('app_promo_remittance_winners', array('slug' => $slug), UrlGeneratorInterface::ABSOLUTE_URL))
        ;

        return $this->render('AppBundle:Promo:remittance_winners.html.twig', array(
            'promo' => $promo,
            'categoryWinner' => $categoryWinner,
        ));
    }

    private function _getRemittanceEntryForm($entry, $validate = true)
    {
        $options = array();
        if (!$validate) {
            $options = array('validation_groups' => false);
        }

        $form = $this->createForm(RemittanceEntryType::class, $entry, $options);
        $form
            ->add('agreeTerms', CheckboxType::class, array('label' => 'I agree'))
            ->add('recaptcha', EWZRecaptchaType::class, array(
                'attr' => array(
                    'options' => array(
                        'theme' => 'light',
                        'type'  => 'image',
                        'size'  => 'normal'
                    )
                ),
                'mapped'      => false,
                'constraints' => array(
                    new RecaptchaTrue()
                )
            ))
            ->add('submit', SubmitType::class, array('label' => 'Submit'))
        ;

        return $form;
    }

    public function acceptCookiePolicyAction()
    {
        $response = new JsonResponse();
        $response->headers->setCookie(new Cookie('cookie_policy_accepted', 1));
        $response->setData(array('status' => 1));

        return $response;
    }
}
