<?php

namespace AppBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder; 
use Symfony\Component\DependencyInjection\Reference;

class OverrideServiceCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
	{
        $definition = $container->getDefinition('sonata.formatter.form.type.selector');
        $definition->setClass('Application\Sonata\FormatterBundle\Form\Type\FormatterType');

        $definition = $container->getDefinition('sonata.page.transformer');
        $definition->setClass('Application\Sonata\PageBundle\Entity\Transformer');
    }
}
