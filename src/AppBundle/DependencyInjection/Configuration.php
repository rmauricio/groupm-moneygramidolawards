<?php

namespace AppBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;
use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('app');
        $this->addMenuConfiguration($rootNode);
        return $treeBuilder;
    }

    public function addMenuConfiguration(ArrayNodeDefinition $rootNode)
	{
		$rootNode
            ->children()
                ->arrayNode('menu')
					->useAttributeAsKey('name')
					->prototype('array')
						->children()
							->arrayNode('options')
								->children()
									->arrayNode('childrenAttributes')
										->useAttributeAsKey('name')
										->prototype('scalar')
										->end()
									->end()
									->scalarNode('allow_safe_labels')->defaultValue(false)->end()
									->scalarNode('template')->end()
								->end()
							->end()
							->arrayNode('items')
								->useAttributeAsKey('name')
								->prototype('array')
									->children()
										->scalarNode('label')->defaultValue('')->end()
										->scalarNode('label_callback')->defaultValue('')->end()
										->scalarNode('label_template')->defaultValue('')->end()
										->scalarNode('uri')->defaultValue('')->end()
										->scalarNode('route_name')->defaultValue('')->end()
										->arrayNode('route_parameters')
											->useAttributeAsKey('name')
											->prototype('scalar')
											->defaultValue(array())
											->end()
										->end()
										->scalarNode('safe_label')->defaultValue(false)->end()
										->scalarNode('role')->defaultValue('')->end()
										->scalarNode('icon')->defaultValue('')->end()
										->arrayNode('attributes')
											->useAttributeAsKey('name')
											->prototype('scalar')
											->defaultValue(array())
											->end()
										->end()
										->arrayNode('link_attributes')
											->useAttributeAsKey('name')
											->prototype('scalar')
											->defaultValue(array())
											->end()
										->end()
										->scalarNode('parent')->defaultValue('')->end()
									->end()
								->end()
							->end()
						->end()
					->end()
                ->end()
            ->end()
        ;
	}
}