<?php

namespace AppBundle\Entity\Promo;

use Doctrine\ORM\Mapping as ORM;

/**
 * DiaryEntry
 */
class DiaryEntry extends Entry
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $referenceNumber;

    /**
     * @var string
     */
    private $writeUp;

    /**
     * @var string
     */
    private $firstName;

    /**
     * @var string|null
     */
    private $middleName;

    /**
     * @var string
     */
    private $lastName;

    /**
     * @var int
     */
    private $age;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string|null
     */
    private $mobileNumber;

    /**
     * @var string|null
     */
    private $landlineNumber;

    /**
     * @var \AppBundle\Entity\Promo\Promo
      */
    private $promo;

    /**
     * @var \Application\Sonata\ClassificationBundle\Entity\Category
     */
    private $category;

    /**
     * @var \Application\Sonata\MediaBundle\Entity\Media
     */
    private $image;

    /**
     * @var \Symfony\Component\HttpFoundation\File\UploadedFile
     */
    private $imageFile;

    /**
     * @var \Application\Sonata\MediaBundle\Entity\Media
     */
    private $video;

    /**
     * @var \Symfony\Component\HttpFoundation\File\UploadedFile
     */
    private $videoFile;

    /**
     * @var \Application\Sonata\MediaBundle\Entity\Media
     */
    private $thumbnail;

    /**
     * @var \Application\Sonata\MediaBundle\Entity\Media
     */
    private $finalImage;

    /**
     * @var \Application\Sonata\MediaBundle\Entity\Media
     */
    private $finalImageMobile;

    /**
     * @var \Application\Sonata\MediaBundle\Entity\Media
     */
    private $finalVideo;

    /**
     * @var string
     */
    private $slug;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    private $votes;

    private $votesCount = 0;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var bool
     */
    private $agreeTerms;

    /**
     * @var \DateTime
     */
    private $updatedAt;

    public function __construct()
    {
        $this->votes = new \Doctrine\Common\Collections\ArrayCollection;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set referenceNumber.
     *
     * @param string $referenceNumber
     *
     * @return RemittanceEntry
     */
    public function setReferenceNumber($referenceNumber)
    {
        $this->referenceNumber = $referenceNumber;

        return $this;
    }

    /**
     * Get referenceNumber.
     *
     * @return string
     */
    public function getReferenceNumber()
    {
        return $this->referenceNumber;
    }

    /**
     * Set writeUp.
     *
     * @param string $writeUp
     *
     * @return DiaryEntry
     */
    public function setWriteUp($writeUp)
    {
        $this->writeUp = $writeUp;

        return $this;
    }

    /**
     * Get writeUp.
     *
     * @return string
     */
    public function getWriteUp()
    {
        return $this->writeUp;
    }

    /**
     * Set firstName.
     *
     * @param string $firstName
     *
     * @return DiaryEntry
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName.
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set middleName.
     *
     * @param string|null $middleName
     *
     * @return DiaryEntry
     */
    public function setMiddleName($middleName = null)
    {
        $this->middleName = $middleName;

        return $this;
    }

    /**
     * Get middleName.
     *
     * @return string|null
     */
    public function getMiddleName()
    {
        return $this->middleName;
    }

    /**
     * Set lastName.
     *
     * @param string $lastName
     *
     * @return DiaryEntry
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName.
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set age.
     *
     * @param int $age
     *
     * @return DiaryEntry
     */
    public function setAge($age)
    {
        $this->age = $age;

        return $this;
    }

    /**
     * Get age.
     *
     * @return int
     */
    public function getAge()
    {
        return $this->age;
    }

    /**
     * Set email.
     *
     * @param string $email
     *
     * @return DiaryEntry
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email.
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set mobileNumber.
     *
     * @param string|null $mobileNumber
     *
     * @return DiaryEntry
     */
    public function setMobileNumber($mobileNumber = null)
    {
        $this->mobileNumber = $mobileNumber;

        return $this;
    }

    /**
     * Get mobileNumber.
     *
     * @return string|null
     */
    public function getMobileNumber()
    {
        return $this->mobileNumber;
    }

    /**
     * Set landlineNumber.
     *
     * @param string|null $landlineNumber
     *
     * @return DiaryEntry
     */
    public function setLandlineNumber($landlineNumber = null)
    {
        $this->landlineNumber = $landlineNumber;

        return $this;
    }

    /**
     * Get landlineNumber.
     *
     * @return string|null
     */
    public function getLandlineNumber()
    {
        return $this->landlineNumber;
    }

    /**
     * Set promo
     *
     * @param \AppBundle\Entity\Promo\Promo $category
     *
     * @return DiaryEntry
     */
    public function setPromo(\AppBundle\Entity\Promo\Promo $promo = null)
    {
        $this->promo = $promo;

        return $this;
    }

    /**
     * Get promo
     *
     * @return \AppBundle\Entity\Promo\Promo
     */
    public function getPromo()
    {
        return $this->promo;
    }

    /**
     * Set category
     *
     * @param \Application\Sonata\ClassificationBundle\Entity\Category $category
     *
     * @return DiaryEntry
     */
    public function setCategory(\Application\Sonata\ClassificationBundle\Entity\Category $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \Application\Sonata\ClassificationBundle\Entity\Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set image
     *
     * @param \Application\Sonata\MediaBundle\Entity\Media $image
     *
     * @return DiaryEntry
     */
    public function setImage(\Application\Sonata\MediaBundle\Entity\Media $image = null)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Set imageFile
     *
     * @param \Symfony\Component\HttpFoundation\File\UploadedFile $imageFile
     * @return DiaryEntry
     */
    public function setImageFile($imageFile = null)
    {
        $this->imageFile = $imageFile;
    }

    /**
     * Get imageFile
     *
     * @return \Symfony\Component\HttpFoundation\File\UploadedFile
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * Set videoFile
     *
     * @param \Symfony\Component\HttpFoundation\File\UploadedFile $videoFile
     * @return DiaryEntry
     */
    public function setVideoFile($videoFile = null)
    {
        $this->videoFile = $videoFile;
    }

    /**
     * Get videoFile
     *
     * @return \Symfony\Component\HttpFoundation\File\UploadedFile
     */
    public function getVideoFile()
    {
        return $this->videoFile;
    }

    /**
     * Get image
     *
     * @return \Application\Sonata\MediaBundle\Entity\Media
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set video
     *
     * @param \Application\Sonata\MediaBundle\Entity\Media $video
     *
     * @return DiaryEntry
     */
    public function setVideo(\Application\Sonata\MediaBundle\Entity\Media $video = null)
    {
        $this->video = $video;

        return $this;
    }

    /**
     * Get video
     *
     * @return \Application\Sonata\MediaBundle\Entity\Media
     */
    public function getVideo()
    {
        return $this->video;
    }

    /**
     * Set thumbnail
     *
     * @param \Application\Sonata\MediaBundle\Entity\Media $thumbnail
     *
     * @return DiaryEntry
     */
    public function setThumbnail(\Application\Sonata\MediaBundle\Entity\Media $thumbnail = null)
    {
        $this->thumbnail = $thumbnail;

        return $this;
    }

    /**
     * Get thumbnail
     *
     * @return \Application\Sonata\MediaBundle\Entity\Media
     */
    public function getThumbnail()
    {
        return $this->thumbnail;
    }

    /**
     * Set finalImage
     *
     * @param \Application\Sonata\MediaBundle\Entity\Media $finalImage
     *
     * @return DiaryEntry
     */
    public function setFinalImage(\Application\Sonata\MediaBundle\Entity\Media $finalImage = null)
    {
        $this->finalImage = $finalImage;

        return $this;
    }

    /**
     * Get finalImage
     *
     * @return \Application\Sonata\MediaBundle\Entity\Media
     */
    public function getFinalImage()
    {
        return $this->finalImage;
    }

    /**
     * Set finalImageMobile
     *
     * @param \Application\Sonata\MediaBundle\Entity\Media $finalImageMobile
     *
     * @return DiaryEntry
     */
    public function setFinalImageMobile(\Application\Sonata\MediaBundle\Entity\Media $finalImageMobile = null)
    {
        $this->finalImageMobile = $finalImageMobile;

        return $this;
    }

    /**
     * Get finalImageMobile
     *
     * @return \Application\Sonata\MediaBundle\Entity\Media
     */
    public function getFinalImageMobile()
    {
        return $this->finalImageMobile;
    }

    /**
     * Set finalVideo
     *
     * @param \Application\Sonata\MediaBundle\Entity\Media $finalVideo
     *
     * @return DiaryEntry
     */
    public function setFinalVideo(\Application\Sonata\MediaBundle\Entity\Media $finalVideo = null)
    {
        $this->finalVideo = $finalVideo;

        return $this;
    }

    /**
     * Get finalVideo
     *
     * @return \Application\Sonata\MediaBundle\Entity\Media
     */
    public function getFinalVideo()
    {
        return $this->finalVideo;
    }

    /**
     * Add vote
     *
     * @param \AppBundle\Entity\Promo\DiaryEntryVote $vote
     *
     * @return DiaryEntry
     */
    public function addDocument(\AppBundle\Entity\Promo\DiaryEntryVote $vote)
    {
        $vote->setDiaryEntry($this);
        $this->votes[] = $vote;

        return $this;
    }

    /**
     * Remove vote
     *
     * @param \AppBundle\Entity\Promo\DiaryEntryVote $vote
     */
    public function removeDocument(\AppBundle\Entity\Promo\DiaryEntryVote $vote)
    {
        $this->votes->removeElement($vote);
        $vote->setDiaryEntry(null);
    }

    /**
     * Get votes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVotes()
    {
        return $this->votes;
    }

    /**
     * Set slug.
     *
     * @param string $slug
     *
     * @return Promo
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug.
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set votesCount.
     *
     * @param integer $votesCount
     *
     * @return Promo
     */
    public function setVotesCount($votesCount)
    {
        $this->votesCount = $slug;

        return $this;
    }

    /**
     * Get votesCount.
     *
     * @return string
     */
    public function getVotesCount()
    {
        return $this->votesCount;
    }

    /**
     * Set agreeTerms
     *
     * @param bool $agreeTerms
     *
     * @return DiaryEntry
     */
    public function setAgreeTerms($agreeTerms = null)
    {
        $this->agreeTerms = $agreeTerms;
    }

    /**
     * Get agreeTerms
     *
     * @return bool
     */
    public function getAgreeTerms()
    {
        return $this->agreeTerms;
    }

    public function getFormattedName()
    {
        return sprintf('%s %s', $this->firstName, $this->lastName);
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return DiaryEntry
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime $updatedAt
     *
     * @return DiaryEntry
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @ORM\PrePersist
     */
    public function prePersist()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function preUpdate()
    {
        $this->updatedAt = new \DateTime();
    }
    
    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getId() ? $this->getReferenceNumber() : 'New Diary Entry';
    }
}
