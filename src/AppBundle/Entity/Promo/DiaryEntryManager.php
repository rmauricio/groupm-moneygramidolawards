<?php 

namespace AppBundle\Entity\Promo;

use Sonata\CoreBundle\Model\BaseEntityManager;
use AppBundle\Entity\Promo\EntryInterface;

/**
 * DiaryEntryManager
 *
 * @author Ryan Mauricio <ryanmarkrmauricio@gmail.com>
 */
class DiaryEntryManager extends BaseEntityManager
{
	public function findOneByExcludeId($criteria, $id) 
	{
		$queryBuilder = $this->getEntityManager()->createQueryBuilder();
		$queryBuilder
			->select('e')
			->from($this->getClass(), 'e')
			->where('e.id != :id')
			->setParameter('id', $id);

		foreach ($criteria as $key => $value) {
			$queryBuilder
	        	->andWhere(sprintf('e.%s = :%s', $key, $key))
	        	->setParameter($key, $value)
	        ;
		}

		$queryBuilder
		   ->add('orderBy', 'e.id DESC')
		   ->setFirstResult(0)
		   ->setMaxResults(1)
		;

		return $queryBuilder->getQuery()->getOneOrNullResult();
	}

	public function findRelated($entry, $limit = 3) 
	{
		$query = $this->getEntityManager()->createQuery(
			    'SELECT e
			    FROM AppBundle:Promo\DiaryEntry e
			    WHERE e.promo = :promo
			    AND e.id != :entry
			    AND e.status = :status
			    ORDER BY RAND()'
			)
			->setParameter('promo', $entry->getPromo())
			->setParameter('entry', $entry)
			->setParameter('status', EntryInterface::STATUS_PUBLISHED)
			->setMaxResults($limit)
		;

		return $query->getResult();
	}

	public function findByCategories($promo, $categories, $limit = 3) 
	{
		$queryBuilder = $this->getEntityManager()->createQueryBuilder();
		$queryBuilder
			->select('e')
			->from($this->getClass(), 'e')
		;

		if (!empty($categories)) {
			$queryBuilder->join('e.category', 'c');
		}

		$queryBuilder
			->where('e.promo = :promo')
			->andWhere('e.status = :status')
			->orderBy('e.position', 'ASC')
			->addOrderBy('e.title', 'ASC')
		;

		if (!empty($categories)) {
			$queryBuilder
				->andWhere('c.id IN (:categories)')
				->addOrderBy('c.position', 'ASC')
				->setParameter('categories', $categories)
			;
		}
		$queryBuilder
			->setParameter('promo', $promo)
			->setParameter('status', EntryInterface::STATUS_PUBLISHED)
			->setMaxResults($limit)
		;

		return $queryBuilder->getQuery()->getResult();
	}

	public function updateVotesCount($entry, $decrement = false)
	{
		if ($decrement) {
			$query = $this->getEntityManager()->createQuery(
				    'UPDATE AppBundle:Promo\DiaryEntry e
				    SET e.votesCount = e.votesCount - 1
				    WHERE e.id = :entry'
				)
				->setParameter('entry', $entry->getId())
			;
		} else {
			$query = $this->getEntityManager()->createQuery(
				    'UPDATE AppBundle:Promo\DiaryEntry e
				    SET e.votesCount = e.votesCount + 1
				    WHERE e.id = :entry'
				)
				->setParameter('entry', $entry->getId())
			;
		}

		return $query->getResult();
	}
}
