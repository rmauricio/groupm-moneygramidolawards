<?php

namespace AppBundle\Entity\Promo;

use Doctrine\ORM\Mapping as ORM;

/**
 * DiaryEntryVote
 */
class DiaryEntryVote
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $email;

    /**
     * @var \AppBundle\Entity\Promo\DiaryEntry
     */
    private $diaryEntry;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \DateTime
     */
    private $updatedAt;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set email.
     *
     * @param string $email
     *
     * @return DiaryEntryVote
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email.
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set diaryEntry
     *
     * @param \AppBundle\Entity\Promo\DiaryEntry $diaryEntry
     *
     * @return DiaryEntryVote
     */
    public function setDiaryEntry(\AppBundle\Entity\Promo\DiaryEntry $diaryEntry = null)
    {
        $this->diaryEntry = $diaryEntry;

        return $this;
    }

    /**
     * Get diaryEntry
     *
     * @return \AppBundle\Entity\Promo\DiaryEntry
     */
    public function getDiaryEntry()
    {
        return $this->diaryEntry;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return DiaryEntryVote
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime $updatedAt
     *
     * @return DiaryEntryVote
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @ORM\PrePersist
     */
    public function prePersist()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function preUpdate()
    {
        $this->updatedAt = new \DateTime();
    }
    
    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getId() ? $this->getEmail() : 'New Diary Entry Vote';
    }
}
