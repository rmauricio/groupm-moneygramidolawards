<?php

namespace AppBundle\Entity\Promo;

abstract class Entry implements EntryInterface
{
	/**
     * @var string
     */
    protected $status = self::STATUS_FOR_REVIEW;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $abstract;

    /**
     * @var int
     */
    protected $position = 0;

    /**
     * Set title.
     *
     * @param string $title
     *
     * @return Entry
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set abstract.
     *
     * @param string $abstract
     *
     * @return Entry
     */
    public function setAbstract($abstract)
    {
        $this->abstract = $abstract;

        return $this;
    }

    /**
     * Get abstract.
     *
     * @return string
     */
    public function getAbstract()
    {
        return $this->abstract;
    }

    /**
     * Set status.
     *
     * @param string $status
     *
     * @return Entry
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status.
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set position.
     *
     * @param string $position
     *
     * @return Entry
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position.
     *
     * @return string
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Get status options.
     *
     * @return array
     */
    public static function getStatusOptions()
    {
    	return array(
    		self::STATUS_FOR_REVIEW => 'For Review',
    		self::STATUS_PUBLISHED => 'Published',
    	);
    }

    /**
     * Get status label.
     *
     * @return string
     */
    public function getStatusLabel()
    {
    	$options = self::getStatusOptions();

    	if (array_key_exists($this->status, $options)) {
    		return $options[$this->status];
    	}
    	
    	return null;
    }

    /**
     * Get status options.
     *
     * @return array
     */
    public static function getTypes()
    {
        return array(
            self::TYPE_REMITTANCE,
            self::TYPE_STORY,
        );
    }
}