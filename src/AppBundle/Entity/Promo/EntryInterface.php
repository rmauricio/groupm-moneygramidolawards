<?php

namespace AppBundle\Entity\Promo;

interface EntryInterface 
{
	const STATUS_FOR_REVIEW = 'for_review';

	const STATUS_PUBLISHED = 'published';

	const TYPE_REMITTANCE = 'remittance';
	
	const TYPE_STORY = 'story';
}