<?php

namespace AppBundle\Entity\Promo;

use Doctrine\ORM\Mapping as ORM;

/**
 * Promo
 */
class Promo
{
    const TYPE_REMITTANCE = 'remittance';
    const TYPE_DIARY = 'diary';

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $type = self::TYPE_REMITTANCE;

    /**
     * @var string
     */
    private $optionDisplayName;

    /**
     * @var string
     */
    private $menuDisplayName;

    /**
     * @var \DateTime
     */
    private $publishStart;

    /**
     * @var \DateTime
     */
    private $publishEnd;

    /**
     * @var string
     */
    private $slug;

    /**
     * @var bool
     */
    private $displayWinners;

    /**
     * @var bool
     */
    private $enabled;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \DateTime
     */
    private $updatedAt;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type.
     *
     * @param string $type
     *
     * @return Promo
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type.
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    public static function getTypeOptions()
    {
        return array(
            self::TYPE_REMITTANCE => 'Remittance',
            self::TYPE_DIARY => 'Diary',
        );
    }

    public static function getTypeLabel()
    {
        $types = self::getTypeOptions();

        if (array_key_exists($this->type, $types)) {
            return $types[$this->type];
        }

        return null;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Promo
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set optionDisplayName.
     *
     * @param string $optionDisplayName
     *
     * @return Promo
     */
    public function setOptionDisplayName($optionDisplayName)
    {
        $this->optionDisplayName = $optionDisplayName;

        return $this;
    }

    /**
     * Get optionDisplayName.
     *
     * @return string
     */
    public function getOptionDisplayName()
    {
        return $this->optionDisplayName;
    }

    /**
     * Set menuDisplayName.
     *
     * @param string $menuDisplayName
     *
     * @return Promo
     */
    public function setMenuDisplayName($menuDisplayName)
    {
        $this->menuDisplayName = $menuDisplayName;

        return $this;
    }

    /**
     * Get menuDisplayName.
     *
     * @return string
     */
    public function getMenuDisplayName()
    {
        return $this->menuDisplayName;
    }

    /**
     * Set publishStart.
     *
     * @param \DateTime $publishStart
     *
     * @return Promo
     */
    public function setPublishStart($publishStart)
    {
        $this->publishStart = $publishStart;

        return $this;
    }

    /**
     * Get publishStart.
     *
     * @return \DateTime
     */
    public function getPublishStart()
    {
        return $this->publishStart;
    }

    /**
     * Set publishEnd.
     *
     * @param \DateTime $publishEnd
     *
     * @return Promo
     */
    public function setPublishEnd($publishEnd)
    {
        $this->publishEnd = $publishEnd;

        return $this;
    }

    /**
     * Get publishEnd.
     *
     * @return \DateTime
     */
    public function getPublishEnd()
    {
        return $this->publishEnd;
    }

    /**
     * Set slug.
     *
     * @param string $slug
     *
     * @return Promo
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug.
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set displayWinners.
     *
     * @param bool $displayWinners
     *
     * @return Promo
     */
    public function setDisplayWinners($displayWinners)
    {
        $this->displayWinners = $displayWinners;

        return $this;
    }

    /**
     * Get displayWinners.
     *
     * @return bool
     */
    public function getDisplayWinners()
    {
        return $this->displayWinners;
    }

    /**
     * Set enabled.
     *
     * @param bool $enabled
     *
     * @return Promo
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled.
     *
     * @return bool
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return Promo
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime $updatedAt
     *
     * @return Promo
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @ORM\PrePersist
     */
    public function prePersist()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function preUpdate()
    {
        $this->updatedAt = new \DateTime();
    }
    
    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getId() ? $this->getName() : 'New Promo';
    }
}
