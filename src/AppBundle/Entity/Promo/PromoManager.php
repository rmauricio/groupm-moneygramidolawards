<?php 

namespace AppBundle\Entity\Promo;

use Sonata\CoreBundle\Model\BaseEntityManager;

/**
 * PromoManager
 *
 * @author Ryan Mauricio <ryanmarkrmauricio@gmail.com>
 */
class PromoManager extends BaseEntityManager
{
	public function findOneByExcludeId($criteria, $id) 
	{
		$queryBuilder = $this->getEntityManager()->createQueryBuilder();
		$queryBuilder
			->select('p')
			->from($this->getClass(), 'p')
			->where('p.id != :id')
			->setParameter('id', $id);

		foreach ($criteria as $key => $value) {
			$queryBuilder
	        	->andWhere(sprintf('p.%s = :%s', $key, $key))
	        	->setParameter($key, $value)
	        ;
		}

		$queryBuilder
		   ->add('orderBy', 'p.id DESC')
		   ->setFirstResult(0)
		   ->setMaxResults(1)
		;

		return $queryBuilder->getQuery()->getOneOrNullResult();
	}

	public function findActive($type = Promo::TYPE_REMITTANCE)
	{
		$currentDateTime = new \DateTime;

		$queryBuilder = $this->getEntityManager()->createQueryBuilder();
		$queryBuilder
			->select('p')
			->from($this->getClass(), 'p')
			->where('p.enabled = true')
			->andWhere('p.publishStart <= :currentDateTime')
			->andWhere('p.publishEnd >= :currentDateTime')
			->andWhere('p.type = :type')
			->setParameter('currentDateTime', $currentDateTime->format('Y-m-d H:i:s'))
			->setParameter('type', $type)
		;

		$queryBuilder
		   ->add('orderBy', 'p.publishStart ASC')
		   ->setFirstResult(0)
		   ->setMaxResults(1)
		;

		return $queryBuilder->getQuery()->getOneOrNullResult();
	}

	public function findPublishStartDateConflicts($promo)
	{
		$queryBuilder = $this->getEntityManager()->createQueryBuilder();
		$queryBuilder
			->select('COUNT(p.id)')
			->from($this->getClass(), 'p')
			/*->where($queryBuilder->expr()->andX(
	            $queryBuilder->expr()->lte('p.publishStart', ':publishStart'),
	            $queryBuilder->expr()->gte('p.publishEnd', ':publishStart')
	        ))
			->orWhere($queryBuilder->expr()->andX(
	            $queryBuilder->expr()->lte('p.publishStart', ':publishEnd'),
	            $queryBuilder->expr()->gte('p.publishEnd', ':publishEnd')
	        ))*/
			->andWhere('p.publishStart <= :publishStart')
			->andWhere('p.publishEnd >= :publishStart')
			->andWhere('p.type = :type')
			->setParameter('publishStart', $promo->getPublishStart()->format('Y-m-d H:i:s'))
			->setParameter('type', $promo->getType())
			// ->setParameter('publishEnd', $publishEnd->format('Y-m-d H:i:s'))
		;

		if ($promo->getId()) {
			$queryBuilder
				->andWhere('p.id != :id')
				->setParameter('id', $promo->getId())
			;
		}

		return $queryBuilder->getQuery()->getSingleScalarResult();
	}

	public function findPublishEndDateConflicts($promo)
	{
		$queryBuilder = $this->getEntityManager()->createQueryBuilder();
		$queryBuilder
			->select('COUNT(p.id)')
			->from($this->getClass(), 'p')
			/*->where($queryBuilder->expr()->andX(
	            $queryBuilder->expr()->lte('p.publishStart', ':publishStart'),
	            $queryBuilder->expr()->gte('p.publishEnd', ':publishStart')
	        ))
			->orWhere($queryBuilder->expr()->andX(
	            $queryBuilder->expr()->lte('p.publishStart', ':publishEnd'),
	            $queryBuilder->expr()->gte('p.publishEnd', ':publishEnd')
	        ))*/
			// ->setParameter('publishStart', $publishStart->format('Y-m-d H:i:s'))
			->andWhere('p.publishStart <= :publishEnd')
			->andWhere('p.publishEnd >= :publishEnd')
			->andWhere('p.type = :type')
			->setParameter('publishEnd', $promo->getPublishEnd()->format('Y-m-d H:i:s'))
			->setParameter('type', $promo->getType())
		;

		if ($promo->getId()) {
			$queryBuilder
				->andWhere('p.id != :id')
				->setParameter('id', $promo->getId())
			;
		}

		return $queryBuilder->getQuery()->getSingleScalarResult();
	}
}
