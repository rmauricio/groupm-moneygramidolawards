<?php

namespace AppBundle\Entity\Promo;

use Doctrine\ORM\Mapping as ORM;

/**
 * RemittanceEntry
 */
class RemittanceEntry extends Entry
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var bool
     */
    private $isSender = true;

    /**
     * @var string
     */
    private $referenceNumber;

    /**
     * @var string
     */
    private $transactionNumber;

    /**
     * @var \AppBundle\Entity\Address\Region
     */
    /*private $senderRegion;*/

    /**
     * @var string
     */
    private $firstName;

    /**
     * @var string|null
     */
    private $middleName;

    /**
     * @var string
     */
    private $lastName;

    /**
     * @var int
     */
    private $age;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $mobileNumber;

    /**
     * @var string
     */
    private $landlineNumber;

    /**
     * @var string
     */
    private $country;

    /**
     * @var string
     */
    private $recipientCountry;

    /**
     * @var string
     */
    private $recipientAddress1;

    /**
     * @var string
     */
    private $recipientAddress2;

    /**
     * @var string
     */
    private $recipientAddress3;

    /**
     * @var \AppBundle\Entity\Address\City
     */
    private $recipientCity;

    /**
     * @var \AppBundle\Entity\Address\Region
     */
    private $recipientRegion;

    /**
     * @var string
     */
    private $recipientZipCode;

    /**
     * @var \AppBundle\Entity\Promo\Promo
      */
    private $promo;

    /**
     * @var \Application\Sonata\ClassificationBundle\Entity\Category
     */
    private $category;

    /**
     * @var \Application\Sonata\MediaBundle\Entity\Media
     */
    private $image;

    /**
     * @var \Application\Sonata\MediaBundle\Entity\Media
     */
    private $thumbnail;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var string
     */
    private $updatedAt;

    /**
     * @var bool
     */
    private $agreeTerms;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set isSender.
     *
     * @param bool $isSender
     *
     * @return RemittanceEntry
     */
    public function setIsSender($isSender)
    {
        $this->isSender = $isSender;

        return $this;
    }

    /**
     * Get isSender.
     *
     * @return bool
     */
    public function getIsSender()
    {
        return $this->isSender;
    }

    /**
     * Get isSender label.
     *
     * @return string
     */
    public function getIsSenderLabel()
    {
        return $this->isSender ? 'Sender' : 'Recipient';
    }

    /**
     * Set transactionNumber.
     *
     * @param string $transactionNumber
     *
     * @return RemittanceEntry
     */
    public function setTransactionNumber($transactionNumber)
    {
        $this->transactionNumber = $transactionNumber;

        return $this;
    }

    /**
     * Get transactionNumber.
     *
     * @return string
     */
    public function getTransactionNumber()
    {
        return $this->transactionNumber;
    }

    /**
     * Set referenceNumber.
     *
     * @param string $referenceNumber
     *
     * @return RemittanceEntry
     */
    public function setReferenceNumber($referenceNumber)
    {
        $this->referenceNumber = $referenceNumber;

        return $this;
    }

    /**
     * Get referenceNumber.
     *
     * @return string
     */
    public function getReferenceNumber()
    {
        return $this->referenceNumber;
    }

    /**
     * Set senderRegion.
     *
     * @param \AppBundle\Entity\Address\Region $senderRegion
     *
     * @return RemittanceEntry
     */
    /*public function setSenderRegion(\AppBundle\Entity\Address\Region $senderRegion = null)
    {
        $this->senderRegion = $senderRegion;

        return $this;
    }*/

    /**
     * Get senderRegion.
     *
     * @return \AppBundle\Entity\Address\Region
     */
    /*public function getSenderRegion()
    {
        return $this->senderRegion;
    }*/

    /**
     * Set firstName.
     *
     * @param string $firstName
     *
     * @return RemittanceEntry
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName.
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set middleName.
     *
     * @param string|null $middleName
     *
     * @return RemittanceEntry
     */
    public function setMiddleName($middleName = null)
    {
        $this->middleName = $middleName;

        return $this;
    }

    /**
     * Get middleName.
     *
     * @return string|null
     */
    public function getMiddleName()
    {
        return $this->middleName;
    }

    /**
     * Set lastName.
     *
     * @param string $lastName
     *
     * @return RemittanceEntry
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName.
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set age.
     *
     * @param int $age
     *
     * @return RemittanceEntry
     */
    public function setAge($age)
    {
        $this->age = $age;

        return $this;
    }

    /**
     * Get age.
     *
     * @return int
     */
    public function getAge()
    {
        return $this->age;
    }

    /**
     * Set email.
     *
     * @param string $email
     *
     * @return RemittanceEntry
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email.
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set mobileNumber.
     *
     * @param string $mobileNumber
     *
     * @return RemittanceEntry
     */
    public function setMobileNumber($mobileNumber)
    {
        $this->mobileNumber = $mobileNumber;

        return $this;
    }

    /**
     * Get mobileNumber.
     *
     * @return string
     */
    public function getMobileNumber()
    {
        return $this->mobileNumber;
    }

    /**
     * Set landlineNumber.
     *
     * @param string $landlineNumber
     *
     * @return RemittanceEntry
     */
    public function setLandlineNumber($landlineNumber)
    {
        $this->landlineNumber = $landlineNumber;

        return $this;
    }

    /**
     * Get landlineNumber.
     *
     * @return string
     */
    public function getLandlineNumber()
    {
        return $this->landlineNumber;
    }

    /**
     * Set country.
     *
     * @param string $country
     *
     * @return RemittanceEntry
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country.
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Get country label.
     *
     * @return string
     */
    public function getCountryLabel()
    {
        $names = \Symfony\Component\Intl\Intl::getRegionBundle()->getCountryNames();

        if (array_key_exists($this->country, $names)) {
            return $names[$this->country];
        }

        return null;
    }

    /**
     * Set recipientCountry.
     *
     * @param string $recipientCountry
     *
     * @return RemittanceEntry
     */
    public function setRecipientCountry($recipientCountry)
    {
        $this->recipientCountry = $recipientCountry;

        return $this;
    }

    /**
     * Get recipientCountry.
     *
     * @return string
     */
    public function getRecipientCountry()
    {
        return $this->recipientCountry;
    }

    /**
     * Get recipientCountry label.
     *
     * @return string
     */
    public function getRecipientCountryLabel()
    {
        $names = \Symfony\Component\Intl\Intl::getRegionBundle()->getCountryNames();

        if (array_key_exists($this->recipientCountry, $names)) {
            return $names[$this->recipientCountry];
        }

        return null;
    }

    /**
     * Set recipientAddress1.
     *
     * @param string $recipientAddress1
     *
     * @return RemittanceEntry
     */
    public function setRecipientAddress1($recipientAddress1)
    {
        $this->recipientAddress1 = $recipientAddress1;

        return $this;
    }

    /**
     * Get recipientAddress1.
     *
     * @return string
     */
    public function getRecipientAddress1()
    {
        return $this->recipientAddress1;
    }

    /**
     * Set recipientAddress2.
     *
     * @param string $recipientAddress2
     *
     * @return RemittanceEntry
     */
    public function setRecipientAddress2($recipientAddress2)
    {
        $this->recipientAddress2 = $recipientAddress2;

        return $this;
    }

    /**
     * Get recipientAddress2.
     *
     * @return string
     */
    public function getRecipientAddress2()
    {
        return $this->recipientAddress2;
    }

    /**
     * Set recipientAddress3.
     *
     * @param string $recipientAddress3
     *
     * @return RemittanceEntry
     */
    public function setRecipientAddress3($recipientAddress3)
    {
        $this->recipientAddress3 = $recipientAddress3;

        return $this;
    }

    /**
     * Get recipientAddress3.
     *
     * @return string
     */
    public function getRecipientAddress3()
    {
        return $this->recipientAddress3;
    }

    /**
     * Set recipientCity.
     *
     * @param \AppBundle\Entity\Address\City $recipientCity
     *
     * @return RemittanceEntry
     */
    public function setRecipientCity(\AppBundle\Entity\Address\City $recipientCity = null)
    {
        $this->recipientCity = $recipientCity;

        return $this;
    }

    /**
     * Get recipientCity.
     *
     * @return \AppBundle\Entity\Address\City
     */
    public function getRecipientCity()
    {
        return $this->recipientCity;
    }

    /**
     * Set recipientRegion.
     *
     * @param \AppBundle\Entity\Address\Region $recipientRegion
     *
     * @return RemittanceEntry
     */
    public function setRecipientRegion(\AppBundle\Entity\Address\Region $recipientRegion = null)
    {
        $this->recipientRegion = $recipientRegion;

        return $this;
    }

    /**
     * Get recipientRegion.
     *
     * @return \AppBundle\Entity\Address\Region
     */
    public function getRecipientRegion()
    {
        return $this->recipientRegion;
    }

    /**
     * Set recipientZipCode.
     *
     * @param string $recipientZipCode
     *
     * @return RemittanceEntry
     */
    public function setRecipientZipCode($recipientZipCode)
    {
        $this->recipientZipCode = $recipientZipCode;

        return $this;
    }

    /**
     * Get recipientZipCode.
     *
     * @return string
     */
    public function getRecipientZipCode()
    {
        return $this->recipientZipCode;
    }

    /**
     * Set promo
     *
     * @param \AppBundle\Entity\Promo\Promo $category
     *
     * @return RemittanceEntry
     */
    public function setPromo(\AppBundle\Entity\Promo\Promo $promo = null)
    {
        $this->promo = $promo;

        return $this;
    }

    /**
     * Get promo
     *
     * @return \AppBundle\Entity\Promo\Promo
     */
    public function getPromo()
    {
        return $this->promo;
    }

    /**
     * Set category
     *
     * @param \Application\Sonata\ClassificationBundle\Entity\Category $category
     *
     * @return RemittanceEntry
     */
    public function setCategory(\Application\Sonata\ClassificationBundle\Entity\Category $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \Application\Sonata\ClassificationBundle\Entity\Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set image
     *
     * @param \Application\Sonata\MediaBundle\Entity\Media $image
     *
     * @return RemittanceEntry
     */
    public function setImage(\Application\Sonata\MediaBundle\Entity\Media $image = null)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return \Application\Sonata\MediaBundle\Entity\Media
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set thumbnail
     *
     * @param \Application\Sonata\MediaBundle\Entity\Media $thumbnail
     *
     * @return RemittanceEntry
     */
    public function setThumbnail(\Application\Sonata\MediaBundle\Entity\Media $thumbnail = null)
    {
        $this->thumbnail = $thumbnail;

        return $this;
    }

    /**
     * Get thumbnail
     *
     * @return \Application\Sonata\MediaBundle\Entity\Media
     */
    public function getThumbnail()
    {
        return $this->thumbnail;
    }

    /**
     * Set agreeTerms
     *
     * @param bool $agreeTerms
     *
     * @return RemittanceEntry
     */
    public function setAgreeTerms($agreeTerms = null)
    {
        $this->agreeTerms = $agreeTerms;
    }

    /**
     * Get agreeTerms
     *
     * @return bool
     */
    public function getAgreeTerms()
    {
        return $this->agreeTerms;
    }

    public function getFormattedName()
    {
        return sprintf('%s %s', $this->firstName, $this->lastName);
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return RemittanceEntry
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt.
     *
     * @param string $updatedAt
     *
     * @return RemittanceEntry
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @ORM\PrePersist
     */
    public function prePersist()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function preUpdate()
    {
        $this->updatedAt = new \DateTime();
    }
    
    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getId() ? $this->getTransactionNumber() : 'New Remittance Entry';
    }
}
