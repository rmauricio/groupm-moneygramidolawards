<?php

namespace AppBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;
use AppBundle\Entity\Promo\Promo;
use AppBundle\Entity\Promo\RemittanceEntry;
use AppBundle\Entity\Promo\DiaryEntry;
use AppBundle\Entity\Promo\DiaryEntryVote;
use Keygen\Utility\Generator\KeyGenerator;
use Cocur\Slugify\Slugify;

class EntrySubscriber implements EventSubscriber
{
    protected $remittanceEntryManager;

    protected $diaryEntryManager;

    public function getSubscribedEvents()
    {
        return [
            Events::prePersist,
            Events::preUpdate,
            Events::preRemove,
        ];
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        // generate reference numbers for new entries
        $this->generateReferenceNumber($args);
        // generate entry slug
        $this->generateSlug($args);
        // update entry votes count
        $this->updateVotesCount($args);
    }

    public function preUpdate(LifecycleEventArgs $args)
    {
        $this->generateSlug($args);
    }

    public function preRemove(LifecycleEventArgs $args)
    {
        $this->updateVotesCount($args, true);
    }

    public function generateReferenceNumber($args)
    {
        $entity = $args->getObject();

        if ($entity instanceof RemittanceEntry || $entity instanceof DiaryEntry) {
            $keygen = new KeyGenerator('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');
            $keys = $keygen->generateUniqueKeys(7, 1);

            $entityManager = $this->remittanceEntryManager;
            if ($entity instanceof DiaryEntry) {
                $entityManager = $this->diaryEntryManager;
            }
            $entry = $entityManager->findOneBy(array('referenceNumber' => $keys[0]));
            while ($entry) {
                $keys = $keygen->generateUniqueKeys(7, 1);
                $entry = $entityManager->findOneBy(array('referenceNumber' => $keys[0]));
            }
            $entity->setReferenceNumber($keys[0]);
        }
    }

    public function generateSlug($args)
    {
        $entity = $args->getObject();

        if ($entity instanceof DiaryEntry) {
            if ($entity->getId()) {
                $slug = $entity->getSlug();
                if (empty($slug)) {
                    $slugify = new Slugify();
                    $slug = $slugify->slugify($entity->getTitle());
                }
                $entry = $this->diaryEntryManager->findOneByExcludeId(array('slug' => $slug), $entity->getId());
                while ($entry) {
                    $slug = sprintf('%s-%d', $entry->getSlug(), 1);
                    $slugCheck = $slugify->slugify($entry->getTitle());
                    if ($slugCheck != $entry->getSlug()) {
                        $slugArray = explode('-', str_replace($slugCheck, '', $entry->getSlug()));
                        $incStart = $slugArray[1];
                        $slug = sprintf('%s-%d', $slugCheck, $incStart + 1);
                    }
                    $entry = $this->diaryEntryManager->findOneByExcludeId(array('slug' => $slug), $entity->getId());
                }
                $entity->setSlug($slug);
            } else {
                $slugify = new Slugify();
                $slug = $slugify->slugify($entity->getTitle());
                $entry = $this->diaryEntryManager->findOneBy(array('slug' => $slug));
                while ($entry) {
                    $slug = sprintf('%s-%d', $entry->getSlug(), 1);
                    $slugCheck = $slugify->slugify($entry->getTitle());
                    if ($slugCheck != $entry->getSlug()) {
                        $slugArray = explode('-', str_replace($slugCheck, '', $entry->getSlug()));
                        $incStart = $slugArray[1];
                        $slug = sprintf('%s-%d', $slugCheck, $incStart + 1);
                    }
                    $entry = $this->diaryEntryManager->findOneBy(array('slug' => $slug));
                }
                $entity->setSlug($slug);
            }
        }
    }

    public function updateVotesCount($args, $decrement = false)
    {
        $entity = $args->getObject();

        if ($entity instanceof DiaryEntryVote) {
            $this->diaryEntryManager->updateVotesCount($entity->getDiaryEntry(), $decrement);
        }
    }

    public function setRemittanceEntryManager($remittanceEntryManager)
    {
        $this->remittanceEntryManager = $remittanceEntryManager;
    }

    public function setDiaryEntryManager($diaryEntryManager)
    {
        $this->diaryEntryManager = $diaryEntryManager;
    }
}