<?php

namespace AppBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\KernelEvents;

use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Cmf\Component\Routing\ChainRouter;
use Symfony\Component\HttpFoundation\RedirectResponse;
use HWI\Bundle\OAuthBundle\Security\Core\Exception\AccountNotLinkedException;
use AppHWIOAuthBundle\Security\Core\Exception\PermissionDeclinedException;
use AppHWIOAuthBundle\Security\Core\Exception\EmailExistException;
use Symfony\Component\Routing\Router;
use AppBundle\Entity\Promo\EntryInterface;
use AppBundle\Security\Core\Exception\InvalidVoteException;
use AppBundle\Security\Core\Exception\AccountAlreadyVotedException;

class OAuthLoginEventSubscriber implements EventSubscriberInterface
{
    protected $sessionChecker;
    protected $sessionTokenStorage;
    protected $sessionAuthUtils;
    protected $isHWIConnect;
    protected $session;
    protected $router;
    protected $diaryEntryManager;
    protected $forceCompleteRegistration;
    
    public function __construct($router, Session $session, $sessionChecker, $sessionTokenStorage, $sessionAuthUtils, $diaryEntryManager, $diaryEntryVoteManager, $isHWIConnect = true, $forceCompleteRegistration = true)
    {
        $this->sessionChecker = $sessionChecker;
        $this->sessionAuthUtils = $sessionAuthUtils;
        $this->sessionTokenStorage = $sessionTokenStorage;
        $this->isHWIConnect = $isHWIConnect;
        $this->session = $session;
        $this->router = $router;
        $this->diaryEntryManager = $diaryEntryManager;
        $this->diaryEntryVoteManager = $diaryEntryVoteManager;
        $this->forceCompleteRegistration = $forceCompleteRegistration;
    }
    
    public static function getSubscribedEvents()
    {
        return array(
            KernelEvents::REQUEST => array(
                array('onOAuthLogin', 8),
            ),
        );
    }

    public function onOAuthLogin(GetResponseEvent $event)
    {
        if (!$event->isMasterRequest()) {
            return;
        }

        if ($this->sessionTokenStorage->getToken() &&
            !$hasUser = $this->sessionChecker->isGranted('IS_AUTHENTICATED_REMEMBERED') &&
            $this->isHWIConnect) {
            $request =  $event->getRequest();
            $error = $this->sessionAuthUtils->getLastAuthenticationError(false);

            if (($error instanceof AccountNotLinkedException || $error instanceof AccountAlreadyVotedException) && $request->get('_route') == 'facebook_connect_fail') {

                $session = $request->getSession();
                $entryVote = $session->get('entryVote');
                
                if ($entryVote) {
                    $entry = $this->diaryEntryManager->findOneBy(array('id' => $entryVote->getId(), 'status' => EntryInterface::STATUS_PUBLISHED));
                    if (!$entry) {
                        $event->setResponse(new RedirectResponse('/'));
                    } elseif ($error instanceof AccountNotLinkedException) {
                        $diaryEntryVote = $this->diaryEntryVoteManager->create();
                        $diaryEntryVote->setDiaryEntry($entry);
                        $diaryEntryVote->setEmail($error->getUsername());
                        $this->diaryEntryVoteManager->save($diaryEntryVote);
                        // flash data for voted entry
                        $session->remove('entryVote');
                        $session->getFlashBag()->add('entryVoted', $entry->getId());
                        $this->sessionAuthUtils->getLastAuthenticationError(true);
                        // redirect to entry page
                        $uri = $this->router->generate('app_promo_diary_view', array('slug' => $entry->getSlug()));
                        $event->setResponse(new RedirectResponse($uri));
                    } elseif ($error instanceof AccountAlreadyVotedException) {
                        // flash data for account already voted
                        $session->getFlashBag()->add('alreadyVoted', $entry->getId());
                        // redirect to entry page
                        $uri = $this->router->generate('app_promo_diary_view', array('slug' => $entry->getSlug()));
                        $event->setResponse(new RedirectResponse($uri));
                    }
                }

                // $event->setResponse(new RedirectResponse('/'));
            } elseif ($error instanceof InvalidVoteException && $request->get('_route') == 'facebook_connect_fail') {
                $event->setResponse(new RedirectResponse('/'));
            }
        }
    }
}