<?php

namespace AppBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;
use AppBundle\Entity\Promo\Promo;
use Cocur\Slugify\Slugify;

class PromoSubscriber implements EventSubscriber
{
    protected $promoManager;

    public function getSubscribedEvents()
    {
        return [
            Events::prePersist,
            Events::preUpdate,
        ];
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $this->generateSlug($args);
    }

    public function preUpdate(LifecycleEventArgs $args)
    {
        $this->generateSlug($args);
    }

    public function generateSlug($args)
    {
        $entity = $args->getObject();

        if ($entity instanceof Promo) {
            if ($entity->getId()) {
                $slug = $entity->getSlug();
                if (empty($slug)) {
                    $slugify = new Slugify();
                    $slug = $slugify->slugify($entity->getName());
                }
                $promo = $this->promoManager->findOneByExcludeId(array('slug' => $slug), $entity->getId());
                while ($promo) {
                    $slug = sprintf('%s-%d', $promo->getSlug(), 1);
                    $slugCheck = $slugify->slugify($promo->getName());
                    if ($slugCheck != $promo->getSlug()) {
                        $slugArray = explode('-', str_replace($slugCheck, '', $promo->getSlug()));
                        $incStart = $slugArray[1];
                        $slug = sprintf('%s-%d', $slugCheck, $incStart + 1);
                    }
                    $promo = $this->promoManager->findOneByExcludeId(array('slug' => $slug), $entity->getId());
                }
                $entity->setSlug($slug);
            } else {
                $slugify = new Slugify();
                $slug = $slugify->slugify($entity->getName());
                $promo = $this->promoManager->findOneBy(array('slug' => $slug));
                while ($promo) {
                    $slug = sprintf('%s-%d', $promo->getSlug(), 1);
                    $slugCheck = $slugify->slugify($promo->getName());
                    if ($slugCheck != $promo->getSlug()) {
                        $slugArray = explode('-', str_replace($slugCheck, '', $promo->getSlug()));
                        $incStart = $slugArray[1];
                        $slug = sprintf('%s-%d', $slugCheck, $incStart + 1);
                    }
                    $promo = $this->promoManager->findOneBy(array('slug' => $slug));
                }
                $entity->setSlug($slug);
            }
        }
    }

    public function setPromoManager($promoManager)
    {
        $this->promoManager = $promoManager;
    }
}