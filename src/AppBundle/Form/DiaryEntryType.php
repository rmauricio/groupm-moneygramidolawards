<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;
use AppBundle\Entity\Promo\DiaryEntry;
use Sonata\MediaBundle\Form\Type\MediaType;

class DiaryEntryType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('imageFile', FileType::class, array())
            // ->add('videoFile', FileType::class, array())
            ->add('title')
            ->add('writeUp', TextareaType::class, array())
            ->add('firstName')
            ->add('middleName')
            ->add('lastName')
            ->add('age', TextType::class, array())
            ->add('email', TextType::class, array())
            ->add('mobileNumber', TextType::class, array())
            ->add('landlineNumber', TextType::class, array())
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
	{
	    $resolver->setDefaults([
	        'data_class' => DiaryEntry::class,
	        'validation_groups' => array('Default', 'AppEntry')
	    ]);
	}
}