<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use AppBundle\Entity\Promo\RemittanceEntry;
use AppBundle\Entity\Address\Region;

class RemittanceEntryType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('isSender', ChoiceType::class, array('choices' => array('Sender' => 1, 'Receiver' => 0), 'expanded' => true))
            ->add('transactionNumber')
            /*->add('senderRegion', null, array('empty_data' => null, 'placeholder' => '', 'query_builder' => function (EntityRepository $er) {
		        return $er->createQueryBuilder('r')
		        	->where('r.enabled = true')
		            ->orderBy('r.name', 'ASC');
		    }, 'choice_label' => function ($region) {
		        return $region->getDisplayName();
		    }))*/
            ->add('firstName')
            ->add('middleName')
            ->add('lastName')
            ->add('age', TextType::class, array())
            ->add('email', TextType::class, array())
            ->add('mobileNumber', TextType::class, array())
            ->add('landlineNumber', TextType::class, array())
            ->add('country', CountryType::class, array('empty_data' => null, 'placeholder' => ''))
            ->add('recipientCountry', CountryType::class, array('empty_data' => null, 'placeholder' => ''))
            ->add('recipientAddress1')
            ->add('recipientAddress2')
            ->add('recipientAddress3')
            /*->add('recipientRegion', null, array('empty_data' => null, 'placeholder' => '', 'query_builder' => function (EntityRepository $er) {
		        return $er->createQueryBuilder('r')
		        	->where('r.enabled = true')
		            ->orderBy('r.name', 'ASC');
		    }, 'choice_label' => function ($region) {
		        return $region->getDisplayName();
		    }))
            ->add('recipientCity', null, array('empty_data' => null, 'placeholder' => '', 'query_builder' => function (EntityRepository $er) {
		        return $er->createQueryBuilder('c')
		        	->where('c.enabled = true')
		            ->orderBy('c.name', 'ASC');
		    }))*/
            ->add('recipientZipCode')
        ;

        $builder->addEventListener(FormEvents::PRE_SET_DATA, array($this, 'onPreSetData'));
        $builder->addEventListener(FormEvents::PRE_SUBMIT, array($this, 'onPreSubmit'));
    }

    public function onPreSetData(FormEvent $event)
    {
        $form = $event->getForm();
        $data = $event->getData();

        $this->addElements($form, $data);
    }

    public function onPreSubmit(FormEvent $event)
    {
        $data = $event->getData();
        $form = $event->getForm();

        $this->addElements($form, $data);
    }

    protected function addElements(FormInterface $form, $data) 
    {
        // Build recipientRegion
        $recipientCountry = null;
        if (is_array($data) && isset($data['recipientCountry'])) {
            $recipientCountry = $data['recipientCountry'];
        } elseif ($data instanceof RemittanceEntry) {
            $recipientCountry = $data->getRecipientCountry();
        }

        if ('PH' !== $recipientCountry) {
            $form->add('recipientRegion', null, array('empty_data' => null, 'placeholder' => '', 'choices' => array()));
        } else {
            $form->add('recipientRegion', null, array('empty_data' => null, 'placeholder' => '', 'query_builder' => function (EntityRepository $er) {
                return $er->createQueryBuilder('r')
                    ->where('r.enabled = true')
                    ->orderBy('r.name', 'ASC');
            }, 'choice_label' => function ($region) {
                return $region->getDisplayName();
            }));
        }

        // Build recipientCity
        $recipientRegion = null;
        if (is_array($data) && isset($data['recipientRegion'])) {
            $recipientRegion = $data['recipientRegion'];
        } elseif ($data instanceof RemittanceEntry) {
            $recipientRegion = $data->getRecipientRegion();
        }

        if (null === $recipientRegion) {
            $form->add('recipientCity', null, array('empty_data' => null, 'placeholder' => '', 'choices' => array()));
        } else {
            $form->add('recipientCity', null, array('empty_data' => null, 'placeholder' => '', 'query_builder' => function (EntityRepository $er) use ($recipientRegion) {
                return $er->createQueryBuilder('c')
                    ->where('c.enabled = true')
                    ->andWhere('c.region = :region')
                    ->orderBy('c.name', 'ASC')
                    ->setParameter('region', $recipientRegion);
            }));
        }
    }

    public function configureOptions(OptionsResolver $resolver)
	{
	    $resolver->setDefaults([
	        'data_class' => RemittanceEntry::class,
	        'validation_groups' => array('Default', 'AppEntry')
	    ]);
	}
}