<?php

namespace AppBundle\Menu;

use Knp\Menu\FactoryInterface;
use AppBundle\Entity\Promo\Promo;

class MenuBuilder
{
    private $factory;

    private $requestStack;

    private $router;

    private $securityAuthorizationChecker;

    private $securityTokenStorage;

    private $promoManager;

    private $items;

    /**
     * @param FactoryInterface $factory
     *
     * Add any other dependency you need
     */
    public function __construct(FactoryInterface $factory, $requestStack, $router, $securityAuthorizationChecker, $securityTokenStorage, $promoManager, array $items)
    {
        $this->factory = $factory;
        $this->requestStack = $requestStack;
        $this->router = $router;
        $this->securityAuthorizationChecker = $securityAuthorizationChecker;
        $this->securityTokenStorage = $securityTokenStorage;
        $this->promoManager = $promoManager;
        $this->items = $items;
    }

    /**
     * Creates the main menu
     *
     * @return \Knp\Menu\ItemInterface
     */
    public function mainMenu()
    {
        return $this->generateMenu('main');
    }

    /**
     * Creates the main menu
     *
     * @return \Knp\Menu\ItemInterface
     */
    public function mainMenu01()
    {
        return $this->generateMenu('main01');
    }

    /**
     * Creates the main mobile menu
     *
     * @return \Knp\Menu\ItemInterface
     */
    public function mainMobileMenu()
    {
        return $this->generateMenu('main_mobile');
    }

    /**
     * Creates the main mobile menu
     *
     * @return \Knp\Menu\ItemInterface
     */
    public function mainMobileMenu01()
    {
        return $this->generateMenu('main_mobile01');
    }
	
	protected function getMenuArray($menuKey)
	{
		return $this->items[$menuKey];
	}
	
	protected function generateMenu($menuKey)
	{
		$menuArray = $this->getMenuArray($menuKey);
		$menuOptions = $menuArray['options'];
		$menu = $this->factory->createItem('main', $menuOptions);
		
		$request = $this->requestStack->getCurrentRequest();
		
		try {
			$routeParams = $this->router->matchRequest($request);
		} catch (\Exception $e) {
			$routeParams = array();
		}

		/*if ($menuKey == 'main' || $menuKey == 'main_mobile') {
			$promos = $this->promoManager->findBy(array('enabled' => true, 'displayWinners' => true, 'type' => Promo::TYPE_REMITTANCE), array('publishStart' => 'ASC'));
			if ($promos) {
				$currentDateTime = new \DateTime();
				foreach ($promos as $promo) {
					$menuArray['items'][$promo->getSlug()] = array(
						'label' => $promo->getMenuDisplayName(),
						'route_name' => 'app_promo_remittance_winners',
						'route_parameters' => array(
							'slug' => $promo->getSlug()
						),
						'safe_label' => false,
					);
				}
			}
		}*/

		$currentRequestUri = $request->getRequestUri();
		foreach ($menuArray['items'] as $menuItemKey => $menuItem) {

			$menuItemParams = array();

			if (!empty($menuItem['role'])) {
				if ('IS_AUTHENTICATED_ANONYMOUSLY' == $menuItem['role'] && $this->securityAuthorizationChecker->isGranted('IS_AUTHENTICATED_FULLY')) {
					continue;
				} elseif (!$this->securityAuthorizationChecker->isGranted($menuItem['role'])) {
					continue;
				}
			}

			if (!empty($menuItem['label'])) {
				$menuItemParams['label'] = $menuItem['label'];
			}
			if (!empty($menuItem['label_callback']) && method_exists($this, $menuItem['label_callback'])) {
				$callbackResult = $this->{$menuItem['label_callback']}();
				$menuItemParams['label'] = sprintf($menuItem['label'], $callbackResult);
			}
			
			$currentItem = false;
			if (!empty($menuItem['uri'])) {
				$menuItemParams['uri'] = $this->router->generate('page_slug', array('path' => $menuItem['uri']));
			} elseif (!empty($menuItem['route_name'])) {
				$menuItemParams['route'] = $menuItem['route_name'];
				if (!empty($menuItem['route_parameters'])) {
					$menuItemParams['routeParameters'] = $menuItem['route_parameters'];
				}
			}
			
			$menuObject = $menu;
			if ($ancestors = $this->getAncestors($menuItem, $menuArray['items'])) {
				for ($i = (count($ancestors) - 1); $i >= 0; $i--) {
					$menuObject = $menuObject[$ancestors[$i]['label']];
				}
			}
			
			// $menuObject->addChild($menuItem['label'], $menuItemParams)->setExtra();;
			if (isset($menuItem['safe_label'])) {
				//$menuObject->addChild($menuItem['label'], $menuItemParams)->setExtra('safe_label', $menuItem['safe_label']);
				$mnuItm = $menuObject->addChild($menuItem['label'], $menuItemParams)->setExtra('safe_label', $menuItem['safe_label']);
			} else {
				//$menuObject->addChild($menuItem['label'], $menuItemParams);
				$mnuItm = $menuObject->addChild($menuItem['label'], $menuItemParams);
			}
			
			$menuItemObject = $menuObject[$menuItem['label']];
			
			if (!empty($menuItem['link_attributes'])) {
				foreach ($menuItem['link_attributes'] as $attributeKey => $attributeValue) {
					$menuItemObject->setLinkAttribute($attributeKey, $attributeValue);
				}
			}

			if (!isset($menuItem['attributes']['class'])) {
				$menuItem['attributes']['class'] = '';
			}
			if (stripos($currentRequestUri, $menuItemObject->getUri()) === 0) {
				$menuItem['attributes']['class'] = $menuItem['attributes']['class'] . ' ancestor';
			}

			if (!empty($menuItem['attributes'])) {
				foreach ($menuItem['attributes'] as $attributeKey => $attributeValue) {
					$menuItemObject->setAttribute($attributeKey, $attributeValue);
				}
			}

			if (isset($menuItem['icon'])) {
				$menuItemObject->setExtra('icon', $menuItem['icon']);
			}
			
			if (isset($menuItem['label_template']) && !empty($menuItem['label_template'])) {
				$menuItemObject->setExtra('label_template', $menuItem['label_template']);
			}
			
			if ($currentRequestUri == $menuItemObject->getUri()) {
				$menuItemObject->setCurrent(true);
			} elseif (stripos($currentRequestUri, $menuItemObject->getUri()) === 0) {
				// $menuItemObject->setAttribute('class', 'ancestor');
			}
		}

        return $menu;
	}
	
	protected function getAncestors($menuItem, $menuItems = array())
	{
		$ancestors = array();
		
		if (!empty($menuItem['parent'])) {
			$parentMenuItem = $menuItems[$menuItem['parent']];
			array_push($ancestors, $parentMenuItem);
			foreach ($this->getAncestors($parentMenuItem, $menuItems) as $key => $ancestor) {
				array_push($ancestors, $ancestor);
			}
		}
		
		return $ancestors;
	}

	private function getCurrentUserName()
	{
		$user = $this->securityTokenStorage->getToken()->getUser();

		if (!$user && !$this->securityAuthorizationChecker->isGranted('IS_AUTHENTICATED_FULLY')) {
			return null;
		}

		return $user->getFirstname();
	}

	private function getCurrentUser()
	{
		return $this->securityTokenStorage->getToken()->getUser();
	}
}