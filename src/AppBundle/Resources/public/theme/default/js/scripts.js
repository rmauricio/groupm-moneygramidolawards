(function ($) {

	$(document).ready(function(){

		// mobile nav show/hide event
		$('header .navbar-toggle').click(function(){
			$('.mobile-nav').addClass('visible');
		});
		$('.mobile-nav .close-trigger').click(function(){
			$('.mobile-nav').removeClass('visible');
		});

		// icheck
		$('input[type="radio"]:not(.icheck-exclude, .icheck-exclude input),input[type="checkbox"]:not(.icheck-exclude, .icheck-exclude input)').iCheck({
            checkboxClass: 'icheckbox_minimal',
            radioClass: 'iradio_minimal'
        });

        // select2
        $('select:not(.select2-exclude select, .select2-exclude)').select2();

        // file upload
        $('.file-upload').each(function(i, o){
        	var upload_btn = $(o).find('.input-group-addon .fa-upload');
        	upload_btn.click(function(){
	            $(this).closest('.file-upload').find('input').trigger('click.bs.fileinput');
	        });
        });

	});

	$.fn.handleFormMultipleSubmission = function() {
		if ($(this).hasClass('processing')) {
			return false;
		}
		$.blockUI({ 
			message: '<img src="/bundles/app/theme/default/images/loader60.svg" />', 
			css: { 
				border: 'none', 
		 		backgroundColor: 'transparent',
		 	}
		});
		$(this).addClass('processing');
	}

	/* fb share */
	$.fn.shareFb = function(link) {
		if (typeof FB !== "undefined") {
	    	FB.ui({
			  method: 'feed',
			  link: link
			}, function(response){
				// callback
			});
	    }
	}

})(jQuery);
