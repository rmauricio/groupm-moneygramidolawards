<?php

namespace AppBundle\Security\Core\Exception;

use Symfony\Component\Security\Core\Exception\AuthenticationException;

/**
 * AccountAlreadyVotedException
 *
 * @author ryan.mauricio
 */
class AccountAlreadyVotedException extends AuthenticationException
{
}