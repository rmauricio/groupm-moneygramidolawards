<?php

namespace AppBundle\Security\Core\Exception;

use Symfony\Component\Security\Core\Exception\AuthenticationException;

/**
 * InvalidVoteException
 *
 * @author ryan.mauricio
 */
class InvalidVoteException extends AuthenticationException
{
}