<?php
namespace AppBundle\Security\Core\User;

use HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface;
use HWI\Bundle\OAuthBundle\Security\Core\User\OAuthUserProvider as BaseOAuthUserProvider;
use Symfony\Component\Security\Core\User\UserInterface;
use HWI\Bundle\OAuthBundle\Security\Core\Exception\AccountNotLinkedException;
use AppBundle\Security\Core\Exception\InvalidVoteException;
use AppBundle\Security\Core\Exception\AccountAlreadyVotedException;

class CustomOAuthUserProvider extends BaseOAuthUserProvider
{
    protected $diaryEntryVoteManager;

    protected $session;

    public function __construct($diaryEntryVoteManager, $session)
    {
        $this->diaryEntryVoteManager = $diaryEntryVoteManager;
        $this->session = $session;
    }

    /**
     * {@inheritDoc}
     */
    /*public function connect(UserInterface $user, UserResponseInterface $response)
    {
        // get property from provider configuration by provider name
        // , it will return `facebook_id` in that case (see service definition below)
        $property = $this->getProperty($response);
        $username = $response->getUsername(); // get the unique user identifier

        //we "disconnect" previously connected users
        $existingUser = $this->userManager->findUserBy(array($property => $username));
        if (null !== $existingUser) {
            // set current user id and token to null for disconnect
            // ...

            $this->userManager->updateUser($existingUser);
        }
        // we connect current user, set current user id and token
        // ...
        $this->userManager->updateUser($user);
    }*/

    /**
     * {@inheritdoc}
     */
    public function loadUserByOAuthUserResponse(UserResponseInterface $response)
    {
    	$entry = $this->session->get('entryVote');
    	if (!$entry) {
    		throw new InvalidVoteException();
    	}

        $userEmail = $response->getEmail();
        $entryVote = $this->diaryEntryVoteManager->findOneBy(array('email' => $userEmail));

        if (null === $entryVote) {
        	$exception = new AccountNotLinkedException(sprintf("User '%s' not found.", $userEmail));
        	$exception->setUsername($userEmail);
            throw $exception;
        }

        throw new AccountAlreadyVotedException();
        /*if (null === $user) {
            $username = $response->getRealName();
            $user = new User();
            $user->setUsername($username);

            // ... save user to database

            return $user;
        }*/
        // else update access token of existing user
        /*$serviceName = $response->getResourceOwner()->getName();
        $setter = 'set' . ucfirst($serviceName) . 'AccessToken';
        $user->$setter($response->getAccessToken());//update access token*/

        // return $this->loadUserByUsername($response->getEmail());
    }
}