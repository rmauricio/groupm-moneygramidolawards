<?php

namespace AppBundle\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class AppExtension extends AbstractExtension
{
	protected $requestStack;

    public function getFunctions()
    {
        return array(
            new TwigFunction('is_cookie_policy_accepted', [$this, 'isCookiePolicyAccepted']),
        );
    }

    public function isCookiePolicyAccepted()
    {
    	$accepted = $this->requestStack->getCurrentRequest()->cookies->get('cookie_policy_accepted');
        return $accepted;
    }

    public function setRequestStack($requestStack)
    {
    	$this->requestStack = $requestStack;
    }
}