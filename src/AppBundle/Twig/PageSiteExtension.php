<?php

namespace AppBundle\Twig;

use Sonata\PageBundle\Site\SiteSelectorInterface;
use Sonata\CoreBundle\Model\ManagerInterface;
use Sonata\PageBundle\Exception\PageNotFoundException;
use Sonata\PageBundle\Model\PageInterface;

class PageSiteExtension extends \Twig_Extension
{
	protected $siteSelector;
	protected $mediaManager;
    protected $siteManager;
    protected $cmsManagerSelector;

    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('page_site_setting', array($this, 'getPageSiteSettings')),
            new \Twig_SimpleFunction('page_site_setting_logo', array($this, 'getPageSiteSettingLogo')),
            new \Twig_SimpleFunction('page_setting', array($this, 'getPageSettings')),
        );
    }

    public function getPageSiteSettings($key = null)
    {
        if (empty($key)) {
            return null;
        }

        $site = $this->getSite();

        return $site->getSetting($key);
    }

    public function getPageSettings($key = null)
    {
        if (empty($key)) {
            return null;
        }

        $page = $this->getPage();
        if (method_exists($page, 'getPage')) {
            $page = $page->getPage();
        }

        return $page->getSetting($key);
    }

    public function getPageSiteSettingLogo()
    {
    	$media = $this->getPageSiteSettings('logo');

    	if (is_int($media)) {
    		$media = $this->mediaManager->findOneBy(['id' => $media]);
    	}

        return $media;
    }

    public function getPage($page = null)
    {
        $cms = $this->cmsManagerSelector->retrieve();
        $site = $this->siteSelector->retrieve();
        $targetPage = false;

        try {
            if (null === $page) {
                $targetPage = $cms->getCurrentPage();
            } elseif ($site && (!$page instanceof PageInterface && is_string($page))) {
                $targetPage = $cms->getInternalRoute($site, $page);
            } elseif ($page instanceof PageInterface) {
                $targetPage = $page;
            }
        } catch (PageNotFoundException $e) {
            // the snapshot does not exist
            $targetPage = false;
        }

        return $targetPage;
    }

    public function setSiteSelector(SiteSelectorInterface $siteSelector) 
    {
    	$this->siteSelector = $siteSelector;
    }

    public function setMediaManager(ManagerInterface $mediaManager) 
    {
    	$this->mediaManager = $mediaManager;
    }

    public function setSiteManager($siteManager)
    {
        $this->siteManager = $siteManager;
    }

    public function setCmsManagerSelector($cmsManagerSelector)
    {
        $this->cmsManagerSelector = $cmsManagerSelector;
    }

    public function getSite()
    {
        $site = $this->siteSelector->retrieve();
        if (!$site) {
            $site = $this->siteManager->findOneBy(['isDefault' => true]);
        }

        return $site;
    }
}