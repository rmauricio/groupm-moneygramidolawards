<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class ValidDiaryEntry extends Constraint
{
    public $messageImageVideoRequired = 'Please upload a family photo or a video story.';
    public $messageMinWriteUpLength = 'This should contain {{ limit }} words or more.';

    // in the base Symfony\Component\Validator\Constraint class
	public function validatedBy()
	{
	    return \get_class($this).'Validator';
	}

	public function getTargets()
	{
	    return self::CLASS_CONSTRAINT;
	}
}