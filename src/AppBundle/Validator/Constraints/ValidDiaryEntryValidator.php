<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class ValidDiaryEntryValidator extends ConstraintValidator
{
	const MIN_WRITEUP_LENGTH = 100;

    public function validate($protocol, Constraint $constraint)
    {
        /*if (empty($protocol->getImageFile()) && empty($protocol->getVideoFile())) {
            $this->context->buildViolation($constraint->messageImageVideoRequired)
                ->addViolation();
        }*/

        if (!empty($protocol->getWriteUp())) {
        	$writeUpArray = preg_split("/\s+|\r\n|\W/", $protocol->getWriteUp());
        	if (count($writeUpArray) < self::MIN_WRITEUP_LENGTH) {
        		$this->context->buildViolation($constraint->messageMinWriteUpLength)
	        		->atPath('writeUp')
	        		->setParameter('{{ limit }}', self::MIN_WRITEUP_LENGTH)
	                ->addViolation();
        	}
        }
    }
}