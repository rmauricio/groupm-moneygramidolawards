<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class ValidPromo extends Constraint
{
    public $messagePublishStartLessThan = 'This value should be less than publish end.';
    public $messagePublishEndGreaterThan = 'This value should be greater than publish start.';
    public $messagePublishStartConflict = 'Publish start date within the publish dates of existing promos is not allowed.';
    public $messagePublishEndConflict = 'Publish end date within the publish dates of existing promos is not allowed.';
    public $messageRequired = 'This value should not be blank.';

    // in the base Symfony\Component\Validator\Constraint class
	public function validatedBy()
	{
	    return \get_class($this).'Validator';
	}

	public function getTargets()
	{
	    return self::CLASS_CONSTRAINT;
	}
}