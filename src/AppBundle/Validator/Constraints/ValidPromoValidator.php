<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class ValidPromoValidator extends ConstraintValidator
{
    private $promoManager;

    public function validate($protocol, Constraint $constraint)
    {
    	if ($protocol->getType() && $protocol->getPublishStart() instanceOf \DateTime && $protocol->getPublishEnd() instanceOf \DateTime) {
        	if ($protocol->getPublishStart()->format('U') > $protocol->getPublishEnd()->format('U')) {
    			$this->context->buildViolation($constraint->messagePublishStartLessThan)
                    ->atPath('publishStart')
                    ->addViolation();
    			$this->context->buildViolation($constraint->messagePublishEndGreaterThan)
                    ->atPath('publishEnd')
                    ->addViolation();
    		}

            $publishStartCheck = $this->promoManager->findPublishStartDateConflicts($protocol);
            if ($publishStartCheck > 0) {
                $this->context->buildViolation($constraint->messagePublishStartConflict)
                    ->atPath('publishStart')
                    ->addViolation();
            }
            $publishEndCheck = $this->promoManager->findPublishEndDateConflicts($protocol);
            if ($publishEndCheck > 0) {
                $this->context->buildViolation($constraint->messagePublishEndConflict)
                    ->atPath('publishEnd')
                    ->addViolation();
            }
        }
    }

    public function setPromoManager($promoManager)
    {
        $this->promoManager = $promoManager;
    }
}