<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class ValidRemittanceEntry extends Constraint
{
    public $messageRequired = 'This value should not be blank.';
    public $contactNumberRequired = 'Please enter mobile number or landline number.';

    // in the base Symfony\Component\Validator\Constraint class
	public function validatedBy()
	{
	    return \get_class($this).'Validator';
	}

	public function getTargets()
	{
	    return self::CLASS_CONSTRAINT;
	}
}