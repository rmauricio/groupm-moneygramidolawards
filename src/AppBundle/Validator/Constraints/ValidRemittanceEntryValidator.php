<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class ValidRemittanceEntryValidator extends ConstraintValidator
{
    public function validate($protocol, Constraint $constraint)
    {
        if ($protocol->getIsSender()) {
            if (empty($protocol->getRecipientCountry())) {
                $this->context->buildViolation($constraint->messageRequired)
                    ->atPath('recipientCountry')
                    ->addViolation();
            } elseif ($protocol->getRecipientCountry() == 'PH') {
                if (empty($protocol->getRecipientAddress1())) {
                    $this->context->buildViolation($constraint->messageRequired)
                        ->atPath('recipientAddress1')
                        ->addViolation();
                }
                if (empty($protocol->getRecipientAddress2())) {
                    $this->context->buildViolation($constraint->messageRequired)
                        ->atPath('recipientAddress2')
                        ->addViolation();
                }
                if (empty($protocol->getRecipientAddress3())) {
                    $this->context->buildViolation($constraint->messageRequired)
                        ->atPath('recipientAddress3')
                        ->addViolation();
                }
                if (empty($protocol->getRecipientRegion())) {
                    $this->context->buildViolation($constraint->messageRequired)
                        ->atPath('recipientRegion')
                        ->addViolation();
                }
                if (empty($protocol->getRecipientCity())) {
                    $this->context->buildViolation($constraint->messageRequired)
                        ->atPath('recipientCity')
                        ->addViolation();
                }
                if (empty($protocol->getRecipientZipCode())) {
                    $this->context->buildViolation($constraint->messageRequired)
                        ->atPath('recipientZipCode')
                        ->addViolation();
                }
            }
        }

        if (empty($protocol->getMobileNumber()) && empty($protocol->getLandlineNumber())) {
            $this->context->buildViolation($constraint->contactNumberRequired)
                ->addViolation();
        }
    }
}