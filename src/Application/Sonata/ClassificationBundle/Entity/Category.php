<?php

namespace Application\Sonata\ClassificationBundle\Entity;

use Sonata\ClassificationBundle\Entity\BaseCategory as BaseCategory;

/**
 * This file has been generated by the SonataEasyExtendsBundle.
 *
 * @link https://sonata-project.org/easy-extends
 *
 * References:
 * @link http://www.doctrine-project.org/projects/orm/2.0/docs/reference/working-with-objects/en
 */
class Category extends BaseCategory
{
    /**
     * @var int $id
     */
    protected $id;

    /**
     * Get id.
     *
     * @return int $id
     */
    public function getId()
    {
        return $this->id;
    }

    public function getTreeOptionLabel()
    {
        $parents = $this->crawlAncestors($this);
        $count = count($parents);
        if ($this->parent) {
            $count--;
        }
        return str_repeat('--', $count) . ' ' . $this->getName();
    }

    public function crawlParents($object)
    {
        $parents = array();

        $parent = $object->parent;
        while ($parent) {
            $parents[] = $parent;
            $parents = array_merge($parents, $this->crawlParents($parent));
        }

        return $parents;
    }

    protected function crawlAncestors($object)
    {
        $ancestors = array();
        
        if (!empty($object->parent)) {
            $parent = $object->parent;
            array_push($ancestors, $parent);
            foreach ($this->crawlAncestors($parent) as $key => $ancestor) {
                array_push($ancestors, $ancestor);
            }
        }
        
        return $ancestors;
    }
}
